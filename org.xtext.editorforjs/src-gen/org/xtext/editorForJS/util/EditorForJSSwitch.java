/**
 */
package org.xtext.editorForJS.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.xtext.editorForJS.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.xtext.editorForJS.EditorForJSPackage
 * @generated
 */
public class EditorForJSSwitch<T> extends Switch<T>
{
  /**
   * The cached model package
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static EditorForJSPackage modelPackage;

  /**
   * Creates an instance of the switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EditorForJSSwitch()
  {
    if (modelPackage == null)
    {
      modelPackage = EditorForJSPackage.eINSTANCE;
    }
  }

  /**
   * Checks whether this is a switch for the given package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @parameter ePackage the package in question.
   * @return whether this is a switch for the given package.
   * @generated
   */
  @Override
  protected boolean isSwitchFor(EPackage ePackage)
  {
    return ePackage == modelPackage;
  }

  /**
   * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the first non-null result returned by a <code>caseXXX</code> call.
   * @generated
   */
  @Override
  protected T doSwitch(int classifierID, EObject theEObject)
  {
    switch (classifierID)
    {
      case EditorForJSPackage.DOMAINMODEL:
      {
        Domainmodel domainmodel = (Domainmodel)theEObject;
        T result = caseDomainmodel(domainmodel);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case EditorForJSPackage.ABSTRACT_ELEMENT:
      {
        AbstractElement abstractElement = (AbstractElement)theEObject;
        T result = caseAbstractElement(abstractElement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case EditorForJSPackage.DECLARATION_DE_FONCTION:
      {
        Declaration_de_fonction declaration_de_fonction = (Declaration_de_fonction)theEObject;
        T result = caseDeclaration_de_fonction(declaration_de_fonction);
        if (result == null) result = caseAbstractElement(declaration_de_fonction);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case EditorForJSPackage.FUNCTION_CONTENT:
      {
        functionContent functionContent = (functionContent)theEObject;
        T result = casefunctionContent(functionContent);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case EditorForJSPackage.TYPE:
      {
        Type type = (Type)theEObject;
        T result = caseType(type);
        if (result == null) result = caseAbstractElement(type);
        if (result == null) result = casefunctionContent(type);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case EditorForJSPackage.INSTRUCTIONS:
      {
        Instructions instructions = (Instructions)theEObject;
        T result = caseInstructions(instructions);
        if (result == null) result = caseAbstractElement(instructions);
        if (result == null) result = casefunctionContent(instructions);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case EditorForJSPackage.INSTANCIATION:
      {
        instanciation instanciation = (instanciation)theEObject;
        T result = caseinstanciation(instanciation);
        if (result == null) result = caseInstructions(instanciation);
        if (result == null) result = caseAbstractElement(instanciation);
        if (result == null) result = casefunctionContent(instanciation);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case EditorForJSPackage.ADDITION:
      {
        addition addition = (addition)theEObject;
        T result = caseaddition(addition);
        if (result == null) result = caseInstructions(addition);
        if (result == null) result = caseAbstractElement(addition);
        if (result == null) result = casefunctionContent(addition);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case EditorForJSPackage.ALERT:
      {
        alert alert = (alert)theEObject;
        T result = casealert(alert);
        if (result == null) result = caseInstructions(alert);
        if (result == null) result = caseAbstractElement(alert);
        if (result == null) result = casefunctionContent(alert);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case EditorForJSPackage.PROMPT:
      {
        prompt prompt = (prompt)theEObject;
        T result = caseprompt(prompt);
        if (result == null) result = caseInstructions(prompt);
        if (result == null) result = caseAbstractElement(prompt);
        if (result == null) result = casefunctionContent(prompt);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      default: return defaultCase(theEObject);
    }
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Domainmodel</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Domainmodel</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDomainmodel(Domainmodel object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Abstract Element</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Abstract Element</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAbstractElement(AbstractElement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Declaration de fonction</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Declaration de fonction</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDeclaration_de_fonction(Declaration_de_fonction object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>function Content</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>function Content</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casefunctionContent(functionContent object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseType(Type object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Instructions</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Instructions</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseInstructions(Instructions object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>instanciation</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>instanciation</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseinstanciation(instanciation object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>addition</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>addition</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseaddition(addition object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>alert</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>alert</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casealert(alert object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>prompt</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>prompt</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseprompt(prompt object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject)
   * @generated
   */
  @Override
  public T defaultCase(EObject object)
  {
    return null;
  }

} //EditorForJSSwitch
