/**
 */
package org.xtext.editorForJS;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>function Content</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.editorForJS.EditorForJSPackage#getfunctionContent()
 * @model
 * @generated
 */
public interface functionContent extends EObject
{
} // functionContent
