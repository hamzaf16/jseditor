/**
 */
package org.xtext.editorForJS;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>prompt</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.editorForJS.prompt#getVotre_text <em>Votre text</em>}</li>
 *   <li>{@link org.xtext.editorForJS.prompt#getText_par_defaut <em>Text par defaut</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.editorForJS.EditorForJSPackage#getprompt()
 * @model
 * @generated
 */
public interface prompt extends Instructions
{
  /**
   * Returns the value of the '<em><b>Votre text</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Votre text</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Votre text</em>' attribute.
   * @see #setVotre_text(String)
   * @see org.xtext.editorForJS.EditorForJSPackage#getprompt_Votre_text()
   * @model
   * @generated
   */
  String getVotre_text();

  /**
   * Sets the value of the '{@link org.xtext.editorForJS.prompt#getVotre_text <em>Votre text</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Votre text</em>' attribute.
   * @see #getVotre_text()
   * @generated
   */
  void setVotre_text(String value);

  /**
   * Returns the value of the '<em><b>Text par defaut</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Text par defaut</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Text par defaut</em>' attribute.
   * @see #setText_par_defaut(String)
   * @see org.xtext.editorForJS.EditorForJSPackage#getprompt_Text_par_defaut()
   * @model
   * @generated
   */
  String getText_par_defaut();

  /**
   * Sets the value of the '{@link org.xtext.editorForJS.prompt#getText_par_defaut <em>Text par defaut</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Text par defaut</em>' attribute.
   * @see #getText_par_defaut()
   * @generated
   */
  void setText_par_defaut(String value);

} // prompt
