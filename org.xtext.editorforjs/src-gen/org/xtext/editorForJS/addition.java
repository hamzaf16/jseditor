/**
 */
package org.xtext.editorForJS;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>addition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.editorForJS.addition#getVariable1 <em>Variable1</em>}</li>
 *   <li>{@link org.xtext.editorForJS.addition#getVariable2 <em>Variable2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.editorForJS.EditorForJSPackage#getaddition()
 * @model
 * @generated
 */
public interface addition extends Instructions
{
  /**
   * Returns the value of the '<em><b>Variable1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Variable1</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Variable1</em>' attribute.
   * @see #setVariable1(String)
   * @see org.xtext.editorForJS.EditorForJSPackage#getaddition_Variable1()
   * @model
   * @generated
   */
  String getVariable1();

  /**
   * Sets the value of the '{@link org.xtext.editorForJS.addition#getVariable1 <em>Variable1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Variable1</em>' attribute.
   * @see #getVariable1()
   * @generated
   */
  void setVariable1(String value);

  /**
   * Returns the value of the '<em><b>Variable2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Variable2</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Variable2</em>' attribute.
   * @see #setVariable2(String)
   * @see org.xtext.editorForJS.EditorForJSPackage#getaddition_Variable2()
   * @model
   * @generated
   */
  String getVariable2();

  /**
   * Sets the value of the '{@link org.xtext.editorForJS.addition#getVariable2 <em>Variable2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Variable2</em>' attribute.
   * @see #getVariable2()
   * @generated
   */
  void setVariable2(String value);

} // addition
