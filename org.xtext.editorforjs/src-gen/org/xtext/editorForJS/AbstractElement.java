/**
 */
package org.xtext.editorForJS;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.editorForJS.EditorForJSPackage#getAbstractElement()
 * @model
 * @generated
 */
public interface AbstractElement extends EObject
{
} // AbstractElement
