/**
 */
package org.xtext.editorForJS;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Declaration de fonction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.editorForJS.Declaration_de_fonction#getFunctionName <em>Function Name</em>}</li>
 *   <li>{@link org.xtext.editorForJS.Declaration_de_fonction#getArguments <em>Arguments</em>}</li>
 *   <li>{@link org.xtext.editorForJS.Declaration_de_fonction#getArgument <em>Argument</em>}</li>
 *   <li>{@link org.xtext.editorForJS.Declaration_de_fonction#getElement <em>Element</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.editorForJS.EditorForJSPackage#getDeclaration_de_fonction()
 * @model
 * @generated
 */
public interface Declaration_de_fonction extends AbstractElement
{
  /**
   * Returns the value of the '<em><b>Function Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Function Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Function Name</em>' attribute.
   * @see #setFunctionName(String)
   * @see org.xtext.editorForJS.EditorForJSPackage#getDeclaration_de_fonction_FunctionName()
   * @model
   * @generated
   */
  String getFunctionName();

  /**
   * Sets the value of the '{@link org.xtext.editorForJS.Declaration_de_fonction#getFunctionName <em>Function Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Function Name</em>' attribute.
   * @see #getFunctionName()
   * @generated
   */
  void setFunctionName(String value);

  /**
   * Returns the value of the '<em><b>Arguments</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Arguments</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Arguments</em>' attribute.
   * @see #setArguments(String)
   * @see org.xtext.editorForJS.EditorForJSPackage#getDeclaration_de_fonction_Arguments()
   * @model
   * @generated
   */
  String getArguments();

  /**
   * Sets the value of the '{@link org.xtext.editorForJS.Declaration_de_fonction#getArguments <em>Arguments</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Arguments</em>' attribute.
   * @see #getArguments()
   * @generated
   */
  void setArguments(String value);

  /**
   * Returns the value of the '<em><b>Argument</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Argument</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Argument</em>' attribute list.
   * @see org.xtext.editorForJS.EditorForJSPackage#getDeclaration_de_fonction_Argument()
   * @model unique="false"
   * @generated
   */
  EList<String> getArgument();

  /**
   * Returns the value of the '<em><b>Element</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.editorForJS.functionContent}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Element</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Element</em>' containment reference list.
   * @see org.xtext.editorForJS.EditorForJSPackage#getDeclaration_de_fonction_Element()
   * @model containment="true"
   * @generated
   */
  EList<functionContent> getElement();

} // Declaration_de_fonction
