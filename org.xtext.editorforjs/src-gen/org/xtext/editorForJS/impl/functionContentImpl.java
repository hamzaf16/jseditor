/**
 */
package org.xtext.editorForJS.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.xtext.editorForJS.EditorForJSPackage;
import org.xtext.editorForJS.functionContent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>function Content</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class functionContentImpl extends MinimalEObjectImpl.Container implements functionContent
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected functionContentImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return EditorForJSPackage.Literals.FUNCTION_CONTENT;
  }

} //functionContentImpl
