/**
 */
package org.xtext.editorForJS.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.xtext.editorForJS.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class EditorForJSFactoryImpl extends EFactoryImpl implements EditorForJSFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static EditorForJSFactory init()
  {
    try
    {
      EditorForJSFactory theEditorForJSFactory = (EditorForJSFactory)EPackage.Registry.INSTANCE.getEFactory(EditorForJSPackage.eNS_URI);
      if (theEditorForJSFactory != null)
      {
        return theEditorForJSFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new EditorForJSFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EditorForJSFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case EditorForJSPackage.DOMAINMODEL: return createDomainmodel();
      case EditorForJSPackage.ABSTRACT_ELEMENT: return createAbstractElement();
      case EditorForJSPackage.DECLARATION_DE_FONCTION: return createDeclaration_de_fonction();
      case EditorForJSPackage.FUNCTION_CONTENT: return createfunctionContent();
      case EditorForJSPackage.TYPE: return createType();
      case EditorForJSPackage.INSTRUCTIONS: return createInstructions();
      case EditorForJSPackage.INSTANCIATION: return createinstanciation();
      case EditorForJSPackage.ADDITION: return createaddition();
      case EditorForJSPackage.ALERT: return createalert();
      case EditorForJSPackage.PROMPT: return createprompt();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Domainmodel createDomainmodel()
  {
    DomainmodelImpl domainmodel = new DomainmodelImpl();
    return domainmodel;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AbstractElement createAbstractElement()
  {
    AbstractElementImpl abstractElement = new AbstractElementImpl();
    return abstractElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Declaration_de_fonction createDeclaration_de_fonction()
  {
    Declaration_de_fonctionImpl declaration_de_fonction = new Declaration_de_fonctionImpl();
    return declaration_de_fonction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public functionContent createfunctionContent()
  {
    functionContentImpl functionContent = new functionContentImpl();
    return functionContent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Type createType()
  {
    TypeImpl type = new TypeImpl();
    return type;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Instructions createInstructions()
  {
    InstructionsImpl instructions = new InstructionsImpl();
    return instructions;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public instanciation createinstanciation()
  {
    instanciationImpl instanciation = new instanciationImpl();
    return instanciation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public addition createaddition()
  {
    additionImpl addition = new additionImpl();
    return addition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public alert createalert()
  {
    alertImpl alert = new alertImpl();
    return alert;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public prompt createprompt()
  {
    promptImpl prompt = new promptImpl();
    return prompt;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EditorForJSPackage getEditorForJSPackage()
  {
    return (EditorForJSPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static EditorForJSPackage getPackage()
  {
    return EditorForJSPackage.eINSTANCE;
  }

} //EditorForJSFactoryImpl
