/**
 */
package org.xtext.editorForJS.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.xtext.editorForJS.EditorForJSPackage;
import org.xtext.editorForJS.addition;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>addition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xtext.editorForJS.impl.additionImpl#getVariable1 <em>Variable1</em>}</li>
 *   <li>{@link org.xtext.editorForJS.impl.additionImpl#getVariable2 <em>Variable2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class additionImpl extends InstructionsImpl implements addition
{
  /**
   * The default value of the '{@link #getVariable1() <em>Variable1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVariable1()
   * @generated
   * @ordered
   */
  protected static final String VARIABLE1_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getVariable1() <em>Variable1</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVariable1()
   * @generated
   * @ordered
   */
  protected String variable1 = VARIABLE1_EDEFAULT;

  /**
   * The default value of the '{@link #getVariable2() <em>Variable2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVariable2()
   * @generated
   * @ordered
   */
  protected static final String VARIABLE2_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getVariable2() <em>Variable2</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVariable2()
   * @generated
   * @ordered
   */
  protected String variable2 = VARIABLE2_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected additionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return EditorForJSPackage.Literals.ADDITION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getVariable1()
  {
    return variable1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVariable1(String newVariable1)
  {
    String oldVariable1 = variable1;
    variable1 = newVariable1;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, EditorForJSPackage.ADDITION__VARIABLE1, oldVariable1, variable1));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getVariable2()
  {
    return variable2;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVariable2(String newVariable2)
  {
    String oldVariable2 = variable2;
    variable2 = newVariable2;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, EditorForJSPackage.ADDITION__VARIABLE2, oldVariable2, variable2));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case EditorForJSPackage.ADDITION__VARIABLE1:
        return getVariable1();
      case EditorForJSPackage.ADDITION__VARIABLE2:
        return getVariable2();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case EditorForJSPackage.ADDITION__VARIABLE1:
        setVariable1((String)newValue);
        return;
      case EditorForJSPackage.ADDITION__VARIABLE2:
        setVariable2((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case EditorForJSPackage.ADDITION__VARIABLE1:
        setVariable1(VARIABLE1_EDEFAULT);
        return;
      case EditorForJSPackage.ADDITION__VARIABLE2:
        setVariable2(VARIABLE2_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case EditorForJSPackage.ADDITION__VARIABLE1:
        return VARIABLE1_EDEFAULT == null ? variable1 != null : !VARIABLE1_EDEFAULT.equals(variable1);
      case EditorForJSPackage.ADDITION__VARIABLE2:
        return VARIABLE2_EDEFAULT == null ? variable2 != null : !VARIABLE2_EDEFAULT.equals(variable2);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (variable1: ");
    result.append(variable1);
    result.append(", variable2: ");
    result.append(variable2);
    result.append(')');
    return result.toString();
  }

} //additionImpl
