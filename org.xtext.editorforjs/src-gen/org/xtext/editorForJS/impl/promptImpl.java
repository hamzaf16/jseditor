/**
 */
package org.xtext.editorForJS.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.xtext.editorForJS.EditorForJSPackage;
import org.xtext.editorForJS.prompt;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>prompt</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xtext.editorForJS.impl.promptImpl#getVotre_text <em>Votre text</em>}</li>
 *   <li>{@link org.xtext.editorForJS.impl.promptImpl#getText_par_defaut <em>Text par defaut</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class promptImpl extends InstructionsImpl implements prompt
{
  /**
   * The default value of the '{@link #getVotre_text() <em>Votre text</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVotre_text()
   * @generated
   * @ordered
   */
  protected static final String VOTRE_TEXT_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getVotre_text() <em>Votre text</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVotre_text()
   * @generated
   * @ordered
   */
  protected String votre_text = VOTRE_TEXT_EDEFAULT;

  /**
   * The default value of the '{@link #getText_par_defaut() <em>Text par defaut</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getText_par_defaut()
   * @generated
   * @ordered
   */
  protected static final String TEXT_PAR_DEFAUT_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getText_par_defaut() <em>Text par defaut</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getText_par_defaut()
   * @generated
   * @ordered
   */
  protected String text_par_defaut = TEXT_PAR_DEFAUT_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected promptImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return EditorForJSPackage.Literals.PROMPT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getVotre_text()
  {
    return votre_text;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVotre_text(String newVotre_text)
  {
    String oldVotre_text = votre_text;
    votre_text = newVotre_text;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, EditorForJSPackage.PROMPT__VOTRE_TEXT, oldVotre_text, votre_text));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getText_par_defaut()
  {
    return text_par_defaut;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setText_par_defaut(String newText_par_defaut)
  {
    String oldText_par_defaut = text_par_defaut;
    text_par_defaut = newText_par_defaut;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, EditorForJSPackage.PROMPT__TEXT_PAR_DEFAUT, oldText_par_defaut, text_par_defaut));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case EditorForJSPackage.PROMPT__VOTRE_TEXT:
        return getVotre_text();
      case EditorForJSPackage.PROMPT__TEXT_PAR_DEFAUT:
        return getText_par_defaut();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case EditorForJSPackage.PROMPT__VOTRE_TEXT:
        setVotre_text((String)newValue);
        return;
      case EditorForJSPackage.PROMPT__TEXT_PAR_DEFAUT:
        setText_par_defaut((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case EditorForJSPackage.PROMPT__VOTRE_TEXT:
        setVotre_text(VOTRE_TEXT_EDEFAULT);
        return;
      case EditorForJSPackage.PROMPT__TEXT_PAR_DEFAUT:
        setText_par_defaut(TEXT_PAR_DEFAUT_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case EditorForJSPackage.PROMPT__VOTRE_TEXT:
        return VOTRE_TEXT_EDEFAULT == null ? votre_text != null : !VOTRE_TEXT_EDEFAULT.equals(votre_text);
      case EditorForJSPackage.PROMPT__TEXT_PAR_DEFAUT:
        return TEXT_PAR_DEFAUT_EDEFAULT == null ? text_par_defaut != null : !TEXT_PAR_DEFAUT_EDEFAULT.equals(text_par_defaut);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (votre_text: ");
    result.append(votre_text);
    result.append(", text_par_defaut: ");
    result.append(text_par_defaut);
    result.append(')');
    return result.toString();
  }

} //promptImpl
