/**
 */
package org.xtext.editorForJS.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.xtext.editorForJS.Declaration_de_fonction;
import org.xtext.editorForJS.EditorForJSPackage;
import org.xtext.editorForJS.functionContent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Declaration de fonction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xtext.editorForJS.impl.Declaration_de_fonctionImpl#getFunctionName <em>Function Name</em>}</li>
 *   <li>{@link org.xtext.editorForJS.impl.Declaration_de_fonctionImpl#getArguments <em>Arguments</em>}</li>
 *   <li>{@link org.xtext.editorForJS.impl.Declaration_de_fonctionImpl#getArgument <em>Argument</em>}</li>
 *   <li>{@link org.xtext.editorForJS.impl.Declaration_de_fonctionImpl#getElement <em>Element</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Declaration_de_fonctionImpl extends AbstractElementImpl implements Declaration_de_fonction
{
  /**
   * The default value of the '{@link #getFunctionName() <em>Function Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFunctionName()
   * @generated
   * @ordered
   */
  protected static final String FUNCTION_NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getFunctionName() <em>Function Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFunctionName()
   * @generated
   * @ordered
   */
  protected String functionName = FUNCTION_NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getArguments() <em>Arguments</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getArguments()
   * @generated
   * @ordered
   */
  protected static final String ARGUMENTS_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getArguments() <em>Arguments</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getArguments()
   * @generated
   * @ordered
   */
  protected String arguments = ARGUMENTS_EDEFAULT;

  /**
   * The cached value of the '{@link #getArgument() <em>Argument</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getArgument()
   * @generated
   * @ordered
   */
  protected EList<String> argument;

  /**
   * The cached value of the '{@link #getElement() <em>Element</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getElement()
   * @generated
   * @ordered
   */
  protected EList<functionContent> element;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected Declaration_de_fonctionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return EditorForJSPackage.Literals.DECLARATION_DE_FONCTION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getFunctionName()
  {
    return functionName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFunctionName(String newFunctionName)
  {
    String oldFunctionName = functionName;
    functionName = newFunctionName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, EditorForJSPackage.DECLARATION_DE_FONCTION__FUNCTION_NAME, oldFunctionName, functionName));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getArguments()
  {
    return arguments;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setArguments(String newArguments)
  {
    String oldArguments = arguments;
    arguments = newArguments;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, EditorForJSPackage.DECLARATION_DE_FONCTION__ARGUMENTS, oldArguments, arguments));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getArgument()
  {
    if (argument == null)
    {
      argument = new EDataTypeEList<String>(String.class, this, EditorForJSPackage.DECLARATION_DE_FONCTION__ARGUMENT);
    }
    return argument;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<functionContent> getElement()
  {
    if (element == null)
    {
      element = new EObjectContainmentEList<functionContent>(functionContent.class, this, EditorForJSPackage.DECLARATION_DE_FONCTION__ELEMENT);
    }
    return element;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case EditorForJSPackage.DECLARATION_DE_FONCTION__ELEMENT:
        return ((InternalEList<?>)getElement()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case EditorForJSPackage.DECLARATION_DE_FONCTION__FUNCTION_NAME:
        return getFunctionName();
      case EditorForJSPackage.DECLARATION_DE_FONCTION__ARGUMENTS:
        return getArguments();
      case EditorForJSPackage.DECLARATION_DE_FONCTION__ARGUMENT:
        return getArgument();
      case EditorForJSPackage.DECLARATION_DE_FONCTION__ELEMENT:
        return getElement();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case EditorForJSPackage.DECLARATION_DE_FONCTION__FUNCTION_NAME:
        setFunctionName((String)newValue);
        return;
      case EditorForJSPackage.DECLARATION_DE_FONCTION__ARGUMENTS:
        setArguments((String)newValue);
        return;
      case EditorForJSPackage.DECLARATION_DE_FONCTION__ARGUMENT:
        getArgument().clear();
        getArgument().addAll((Collection<? extends String>)newValue);
        return;
      case EditorForJSPackage.DECLARATION_DE_FONCTION__ELEMENT:
        getElement().clear();
        getElement().addAll((Collection<? extends functionContent>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case EditorForJSPackage.DECLARATION_DE_FONCTION__FUNCTION_NAME:
        setFunctionName(FUNCTION_NAME_EDEFAULT);
        return;
      case EditorForJSPackage.DECLARATION_DE_FONCTION__ARGUMENTS:
        setArguments(ARGUMENTS_EDEFAULT);
        return;
      case EditorForJSPackage.DECLARATION_DE_FONCTION__ARGUMENT:
        getArgument().clear();
        return;
      case EditorForJSPackage.DECLARATION_DE_FONCTION__ELEMENT:
        getElement().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case EditorForJSPackage.DECLARATION_DE_FONCTION__FUNCTION_NAME:
        return FUNCTION_NAME_EDEFAULT == null ? functionName != null : !FUNCTION_NAME_EDEFAULT.equals(functionName);
      case EditorForJSPackage.DECLARATION_DE_FONCTION__ARGUMENTS:
        return ARGUMENTS_EDEFAULT == null ? arguments != null : !ARGUMENTS_EDEFAULT.equals(arguments);
      case EditorForJSPackage.DECLARATION_DE_FONCTION__ARGUMENT:
        return argument != null && !argument.isEmpty();
      case EditorForJSPackage.DECLARATION_DE_FONCTION__ELEMENT:
        return element != null && !element.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (functionName: ");
    result.append(functionName);
    result.append(", arguments: ");
    result.append(arguments);
    result.append(", argument: ");
    result.append(argument);
    result.append(')');
    return result.toString();
  }

} //Declaration_de_fonctionImpl
