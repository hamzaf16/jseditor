/**
 */
package org.xtext.editorForJS;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.xtext.editorForJS.EditorForJSFactory
 * @model kind="package"
 * @generated
 */
public interface EditorForJSPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "editorForJS";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.xtext.org/EditorForJS";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "editorForJS";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  EditorForJSPackage eINSTANCE = org.xtext.editorForJS.impl.EditorForJSPackageImpl.init();

  /**
   * The meta object id for the '{@link org.xtext.editorForJS.impl.DomainmodelImpl <em>Domainmodel</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.editorForJS.impl.DomainmodelImpl
   * @see org.xtext.editorForJS.impl.EditorForJSPackageImpl#getDomainmodel()
   * @generated
   */
  int DOMAINMODEL = 0;

  /**
   * The feature id for the '<em><b>Elements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOMAINMODEL__ELEMENTS = 0;

  /**
   * The number of structural features of the '<em>Domainmodel</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOMAINMODEL_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.editorForJS.impl.AbstractElementImpl <em>Abstract Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.editorForJS.impl.AbstractElementImpl
   * @see org.xtext.editorForJS.impl.EditorForJSPackageImpl#getAbstractElement()
   * @generated
   */
  int ABSTRACT_ELEMENT = 1;

  /**
   * The number of structural features of the '<em>Abstract Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ABSTRACT_ELEMENT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.xtext.editorForJS.impl.Declaration_de_fonctionImpl <em>Declaration de fonction</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.editorForJS.impl.Declaration_de_fonctionImpl
   * @see org.xtext.editorForJS.impl.EditorForJSPackageImpl#getDeclaration_de_fonction()
   * @generated
   */
  int DECLARATION_DE_FONCTION = 2;

  /**
   * The feature id for the '<em><b>Function Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECLARATION_DE_FONCTION__FUNCTION_NAME = ABSTRACT_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Arguments</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECLARATION_DE_FONCTION__ARGUMENTS = ABSTRACT_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Argument</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECLARATION_DE_FONCTION__ARGUMENT = ABSTRACT_ELEMENT_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Element</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECLARATION_DE_FONCTION__ELEMENT = ABSTRACT_ELEMENT_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Declaration de fonction</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DECLARATION_DE_FONCTION_FEATURE_COUNT = ABSTRACT_ELEMENT_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link org.xtext.editorForJS.impl.functionContentImpl <em>function Content</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.editorForJS.impl.functionContentImpl
   * @see org.xtext.editorForJS.impl.EditorForJSPackageImpl#getfunctionContent()
   * @generated
   */
  int FUNCTION_CONTENT = 3;

  /**
   * The number of structural features of the '<em>function Content</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FUNCTION_CONTENT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link org.xtext.editorForJS.impl.TypeImpl <em>Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.editorForJS.impl.TypeImpl
   * @see org.xtext.editorForJS.impl.EditorForJSPackageImpl#getType()
   * @generated
   */
  int TYPE = 4;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE__NAME = ABSTRACT_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TYPE_FEATURE_COUNT = ABSTRACT_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.editorForJS.impl.InstructionsImpl <em>Instructions</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.editorForJS.impl.InstructionsImpl
   * @see org.xtext.editorForJS.impl.EditorForJSPackageImpl#getInstructions()
   * @generated
   */
  int INSTRUCTIONS = 5;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTRUCTIONS__NAME = ABSTRACT_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Instructions</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTRUCTIONS_FEATURE_COUNT = ABSTRACT_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.editorForJS.impl.instanciationImpl <em>instanciation</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.editorForJS.impl.instanciationImpl
   * @see org.xtext.editorForJS.impl.EditorForJSPackageImpl#getinstanciation()
   * @generated
   */
  int INSTANCIATION = 6;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCIATION__NAME = INSTRUCTIONS__NAME;

  /**
   * The feature id for the '<em><b>Variable</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCIATION__VARIABLE = INSTRUCTIONS_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>instanciation</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INSTANCIATION_FEATURE_COUNT = INSTRUCTIONS_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.editorForJS.impl.additionImpl <em>addition</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.editorForJS.impl.additionImpl
   * @see org.xtext.editorForJS.impl.EditorForJSPackageImpl#getaddition()
   * @generated
   */
  int ADDITION = 7;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADDITION__NAME = INSTRUCTIONS__NAME;

  /**
   * The feature id for the '<em><b>Variable1</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADDITION__VARIABLE1 = INSTRUCTIONS_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Variable2</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADDITION__VARIABLE2 = INSTRUCTIONS_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>addition</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ADDITION_FEATURE_COUNT = INSTRUCTIONS_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link org.xtext.editorForJS.impl.alertImpl <em>alert</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.editorForJS.impl.alertImpl
   * @see org.xtext.editorForJS.impl.EditorForJSPackageImpl#getalert()
   * @generated
   */
  int ALERT = 8;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALERT__NAME = INSTRUCTIONS__NAME;

  /**
   * The feature id for the '<em><b>Message</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALERT__MESSAGE = INSTRUCTIONS_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>alert</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALERT_FEATURE_COUNT = INSTRUCTIONS_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.editorForJS.impl.promptImpl <em>prompt</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.editorForJS.impl.promptImpl
   * @see org.xtext.editorForJS.impl.EditorForJSPackageImpl#getprompt()
   * @generated
   */
  int PROMPT = 9;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROMPT__NAME = INSTRUCTIONS__NAME;

  /**
   * The feature id for the '<em><b>Votre text</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROMPT__VOTRE_TEXT = INSTRUCTIONS_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Text par defaut</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROMPT__TEXT_PAR_DEFAUT = INSTRUCTIONS_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>prompt</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PROMPT_FEATURE_COUNT = INSTRUCTIONS_FEATURE_COUNT + 2;


  /**
   * Returns the meta object for class '{@link org.xtext.editorForJS.Domainmodel <em>Domainmodel</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Domainmodel</em>'.
   * @see org.xtext.editorForJS.Domainmodel
   * @generated
   */
  EClass getDomainmodel();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.editorForJS.Domainmodel#getElements <em>Elements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Elements</em>'.
   * @see org.xtext.editorForJS.Domainmodel#getElements()
   * @see #getDomainmodel()
   * @generated
   */
  EReference getDomainmodel_Elements();

  /**
   * Returns the meta object for class '{@link org.xtext.editorForJS.AbstractElement <em>Abstract Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Abstract Element</em>'.
   * @see org.xtext.editorForJS.AbstractElement
   * @generated
   */
  EClass getAbstractElement();

  /**
   * Returns the meta object for class '{@link org.xtext.editorForJS.Declaration_de_fonction <em>Declaration de fonction</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Declaration de fonction</em>'.
   * @see org.xtext.editorForJS.Declaration_de_fonction
   * @generated
   */
  EClass getDeclaration_de_fonction();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.editorForJS.Declaration_de_fonction#getFunctionName <em>Function Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Function Name</em>'.
   * @see org.xtext.editorForJS.Declaration_de_fonction#getFunctionName()
   * @see #getDeclaration_de_fonction()
   * @generated
   */
  EAttribute getDeclaration_de_fonction_FunctionName();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.editorForJS.Declaration_de_fonction#getArguments <em>Arguments</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Arguments</em>'.
   * @see org.xtext.editorForJS.Declaration_de_fonction#getArguments()
   * @see #getDeclaration_de_fonction()
   * @generated
   */
  EAttribute getDeclaration_de_fonction_Arguments();

  /**
   * Returns the meta object for the attribute list '{@link org.xtext.editorForJS.Declaration_de_fonction#getArgument <em>Argument</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Argument</em>'.
   * @see org.xtext.editorForJS.Declaration_de_fonction#getArgument()
   * @see #getDeclaration_de_fonction()
   * @generated
   */
  EAttribute getDeclaration_de_fonction_Argument();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.editorForJS.Declaration_de_fonction#getElement <em>Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Element</em>'.
   * @see org.xtext.editorForJS.Declaration_de_fonction#getElement()
   * @see #getDeclaration_de_fonction()
   * @generated
   */
  EReference getDeclaration_de_fonction_Element();

  /**
   * Returns the meta object for class '{@link org.xtext.editorForJS.functionContent <em>function Content</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>function Content</em>'.
   * @see org.xtext.editorForJS.functionContent
   * @generated
   */
  EClass getfunctionContent();

  /**
   * Returns the meta object for class '{@link org.xtext.editorForJS.Type <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Type</em>'.
   * @see org.xtext.editorForJS.Type
   * @generated
   */
  EClass getType();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.editorForJS.Type#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.editorForJS.Type#getName()
   * @see #getType()
   * @generated
   */
  EAttribute getType_Name();

  /**
   * Returns the meta object for class '{@link org.xtext.editorForJS.Instructions <em>Instructions</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Instructions</em>'.
   * @see org.xtext.editorForJS.Instructions
   * @generated
   */
  EClass getInstructions();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.editorForJS.Instructions#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see org.xtext.editorForJS.Instructions#getName()
   * @see #getInstructions()
   * @generated
   */
  EAttribute getInstructions_Name();

  /**
   * Returns the meta object for class '{@link org.xtext.editorForJS.instanciation <em>instanciation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>instanciation</em>'.
   * @see org.xtext.editorForJS.instanciation
   * @generated
   */
  EClass getinstanciation();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.editorForJS.instanciation#getVariable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Variable</em>'.
   * @see org.xtext.editorForJS.instanciation#getVariable()
   * @see #getinstanciation()
   * @generated
   */
  EAttribute getinstanciation_Variable();

  /**
   * Returns the meta object for class '{@link org.xtext.editorForJS.addition <em>addition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>addition</em>'.
   * @see org.xtext.editorForJS.addition
   * @generated
   */
  EClass getaddition();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.editorForJS.addition#getVariable1 <em>Variable1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Variable1</em>'.
   * @see org.xtext.editorForJS.addition#getVariable1()
   * @see #getaddition()
   * @generated
   */
  EAttribute getaddition_Variable1();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.editorForJS.addition#getVariable2 <em>Variable2</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Variable2</em>'.
   * @see org.xtext.editorForJS.addition#getVariable2()
   * @see #getaddition()
   * @generated
   */
  EAttribute getaddition_Variable2();

  /**
   * Returns the meta object for class '{@link org.xtext.editorForJS.alert <em>alert</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>alert</em>'.
   * @see org.xtext.editorForJS.alert
   * @generated
   */
  EClass getalert();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.editorForJS.alert#getMessage <em>Message</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Message</em>'.
   * @see org.xtext.editorForJS.alert#getMessage()
   * @see #getalert()
   * @generated
   */
  EAttribute getalert_Message();

  /**
   * Returns the meta object for class '{@link org.xtext.editorForJS.prompt <em>prompt</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>prompt</em>'.
   * @see org.xtext.editorForJS.prompt
   * @generated
   */
  EClass getprompt();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.editorForJS.prompt#getVotre_text <em>Votre text</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Votre text</em>'.
   * @see org.xtext.editorForJS.prompt#getVotre_text()
   * @see #getprompt()
   * @generated
   */
  EAttribute getprompt_Votre_text();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.editorForJS.prompt#getText_par_defaut <em>Text par defaut</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Text par defaut</em>'.
   * @see org.xtext.editorForJS.prompt#getText_par_defaut()
   * @see #getprompt()
   * @generated
   */
  EAttribute getprompt_Text_par_defaut();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  EditorForJSFactory getEditorForJSFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link org.xtext.editorForJS.impl.DomainmodelImpl <em>Domainmodel</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.editorForJS.impl.DomainmodelImpl
     * @see org.xtext.editorForJS.impl.EditorForJSPackageImpl#getDomainmodel()
     * @generated
     */
    EClass DOMAINMODEL = eINSTANCE.getDomainmodel();

    /**
     * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOMAINMODEL__ELEMENTS = eINSTANCE.getDomainmodel_Elements();

    /**
     * The meta object literal for the '{@link org.xtext.editorForJS.impl.AbstractElementImpl <em>Abstract Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.editorForJS.impl.AbstractElementImpl
     * @see org.xtext.editorForJS.impl.EditorForJSPackageImpl#getAbstractElement()
     * @generated
     */
    EClass ABSTRACT_ELEMENT = eINSTANCE.getAbstractElement();

    /**
     * The meta object literal for the '{@link org.xtext.editorForJS.impl.Declaration_de_fonctionImpl <em>Declaration de fonction</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.editorForJS.impl.Declaration_de_fonctionImpl
     * @see org.xtext.editorForJS.impl.EditorForJSPackageImpl#getDeclaration_de_fonction()
     * @generated
     */
    EClass DECLARATION_DE_FONCTION = eINSTANCE.getDeclaration_de_fonction();

    /**
     * The meta object literal for the '<em><b>Function Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DECLARATION_DE_FONCTION__FUNCTION_NAME = eINSTANCE.getDeclaration_de_fonction_FunctionName();

    /**
     * The meta object literal for the '<em><b>Arguments</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DECLARATION_DE_FONCTION__ARGUMENTS = eINSTANCE.getDeclaration_de_fonction_Arguments();

    /**
     * The meta object literal for the '<em><b>Argument</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DECLARATION_DE_FONCTION__ARGUMENT = eINSTANCE.getDeclaration_de_fonction_Argument();

    /**
     * The meta object literal for the '<em><b>Element</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DECLARATION_DE_FONCTION__ELEMENT = eINSTANCE.getDeclaration_de_fonction_Element();

    /**
     * The meta object literal for the '{@link org.xtext.editorForJS.impl.functionContentImpl <em>function Content</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.editorForJS.impl.functionContentImpl
     * @see org.xtext.editorForJS.impl.EditorForJSPackageImpl#getfunctionContent()
     * @generated
     */
    EClass FUNCTION_CONTENT = eINSTANCE.getfunctionContent();

    /**
     * The meta object literal for the '{@link org.xtext.editorForJS.impl.TypeImpl <em>Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.editorForJS.impl.TypeImpl
     * @see org.xtext.editorForJS.impl.EditorForJSPackageImpl#getType()
     * @generated
     */
    EClass TYPE = eINSTANCE.getType();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TYPE__NAME = eINSTANCE.getType_Name();

    /**
     * The meta object literal for the '{@link org.xtext.editorForJS.impl.InstructionsImpl <em>Instructions</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.editorForJS.impl.InstructionsImpl
     * @see org.xtext.editorForJS.impl.EditorForJSPackageImpl#getInstructions()
     * @generated
     */
    EClass INSTRUCTIONS = eINSTANCE.getInstructions();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INSTRUCTIONS__NAME = eINSTANCE.getInstructions_Name();

    /**
     * The meta object literal for the '{@link org.xtext.editorForJS.impl.instanciationImpl <em>instanciation</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.editorForJS.impl.instanciationImpl
     * @see org.xtext.editorForJS.impl.EditorForJSPackageImpl#getinstanciation()
     * @generated
     */
    EClass INSTANCIATION = eINSTANCE.getinstanciation();

    /**
     * The meta object literal for the '<em><b>Variable</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INSTANCIATION__VARIABLE = eINSTANCE.getinstanciation_Variable();

    /**
     * The meta object literal for the '{@link org.xtext.editorForJS.impl.additionImpl <em>addition</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.editorForJS.impl.additionImpl
     * @see org.xtext.editorForJS.impl.EditorForJSPackageImpl#getaddition()
     * @generated
     */
    EClass ADDITION = eINSTANCE.getaddition();

    /**
     * The meta object literal for the '<em><b>Variable1</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ADDITION__VARIABLE1 = eINSTANCE.getaddition_Variable1();

    /**
     * The meta object literal for the '<em><b>Variable2</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ADDITION__VARIABLE2 = eINSTANCE.getaddition_Variable2();

    /**
     * The meta object literal for the '{@link org.xtext.editorForJS.impl.alertImpl <em>alert</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.editorForJS.impl.alertImpl
     * @see org.xtext.editorForJS.impl.EditorForJSPackageImpl#getalert()
     * @generated
     */
    EClass ALERT = eINSTANCE.getalert();

    /**
     * The meta object literal for the '<em><b>Message</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ALERT__MESSAGE = eINSTANCE.getalert_Message();

    /**
     * The meta object literal for the '{@link org.xtext.editorForJS.impl.promptImpl <em>prompt</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.editorForJS.impl.promptImpl
     * @see org.xtext.editorForJS.impl.EditorForJSPackageImpl#getprompt()
     * @generated
     */
    EClass PROMPT = eINSTANCE.getprompt();

    /**
     * The meta object literal for the '<em><b>Votre text</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PROMPT__VOTRE_TEXT = eINSTANCE.getprompt_Votre_text();

    /**
     * The meta object literal for the '<em><b>Text par defaut</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute PROMPT__TEXT_PAR_DEFAUT = eINSTANCE.getprompt_Text_par_defaut();

  }

} //EditorForJSPackage
