package org.xtext.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.services.EditorForJSGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalEditorForJSParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'function'", "'('", "','", "'){'", "'}'", "'var'", "';'", "'='", "'+'", "'alert(\"'", "'\");'", "'prompt(\"'"
    };
    public static final int RULE_ID=4;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__19=19;
    public static final int RULE_STRING=6;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=5;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalEditorForJSParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalEditorForJSParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalEditorForJSParser.tokenNames; }
    public String getGrammarFileName() { return "../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g"; }



     	private EditorForJSGrammarAccess grammarAccess;
     	
        public InternalEditorForJSParser(TokenStream input, EditorForJSGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Domainmodel";	
       	}
       	
       	@Override
       	protected EditorForJSGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleDomainmodel"
    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:67:1: entryRuleDomainmodel returns [EObject current=null] : iv_ruleDomainmodel= ruleDomainmodel EOF ;
    public final EObject entryRuleDomainmodel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDomainmodel = null;


        try {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:68:2: (iv_ruleDomainmodel= ruleDomainmodel EOF )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:69:2: iv_ruleDomainmodel= ruleDomainmodel EOF
            {
             newCompositeNode(grammarAccess.getDomainmodelRule()); 
            pushFollow(FOLLOW_ruleDomainmodel_in_entryRuleDomainmodel75);
            iv_ruleDomainmodel=ruleDomainmodel();

            state._fsp--;

             current =iv_ruleDomainmodel; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDomainmodel85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDomainmodel"


    // $ANTLR start "ruleDomainmodel"
    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:76:1: ruleDomainmodel returns [EObject current=null] : ( (lv_elements_0_0= ruleAbstractElement ) )* ;
    public final EObject ruleDomainmodel() throws RecognitionException {
        EObject current = null;

        EObject lv_elements_0_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:79:28: ( ( (lv_elements_0_0= ruleAbstractElement ) )* )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:80:1: ( (lv_elements_0_0= ruleAbstractElement ) )*
            {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:80:1: ( (lv_elements_0_0= ruleAbstractElement ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==RULE_ID||LA1_0==11||LA1_0==16||LA1_0==20||LA1_0==22) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:81:1: (lv_elements_0_0= ruleAbstractElement )
            	    {
            	    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:81:1: (lv_elements_0_0= ruleAbstractElement )
            	    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:82:3: lv_elements_0_0= ruleAbstractElement
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getDomainmodelAccess().getElementsAbstractElementParserRuleCall_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleAbstractElement_in_ruleDomainmodel130);
            	    lv_elements_0_0=ruleAbstractElement();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getDomainmodelRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"elements",
            	            		lv_elements_0_0, 
            	            		"AbstractElement");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDomainmodel"


    // $ANTLR start "entryRuleAbstractElement"
    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:106:1: entryRuleAbstractElement returns [EObject current=null] : iv_ruleAbstractElement= ruleAbstractElement EOF ;
    public final EObject entryRuleAbstractElement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAbstractElement = null;


        try {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:107:2: (iv_ruleAbstractElement= ruleAbstractElement EOF )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:108:2: iv_ruleAbstractElement= ruleAbstractElement EOF
            {
             newCompositeNode(grammarAccess.getAbstractElementRule()); 
            pushFollow(FOLLOW_ruleAbstractElement_in_entryRuleAbstractElement166);
            iv_ruleAbstractElement=ruleAbstractElement();

            state._fsp--;

             current =iv_ruleAbstractElement; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAbstractElement176); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAbstractElement"


    // $ANTLR start "ruleAbstractElement"
    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:115:1: ruleAbstractElement returns [EObject current=null] : (this_Declaration_de_fonction_0= ruleDeclaration_de_fonction | this_Type_1= ruleType | this_Instructions_2= ruleInstructions ) ;
    public final EObject ruleAbstractElement() throws RecognitionException {
        EObject current = null;

        EObject this_Declaration_de_fonction_0 = null;

        EObject this_Type_1 = null;

        EObject this_Instructions_2 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:118:28: ( (this_Declaration_de_fonction_0= ruleDeclaration_de_fonction | this_Type_1= ruleType | this_Instructions_2= ruleInstructions ) )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:119:1: (this_Declaration_de_fonction_0= ruleDeclaration_de_fonction | this_Type_1= ruleType | this_Instructions_2= ruleInstructions )
            {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:119:1: (this_Declaration_de_fonction_0= ruleDeclaration_de_fonction | this_Type_1= ruleType | this_Instructions_2= ruleInstructions )
            int alt2=3;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt2=1;
                }
                break;
            case 16:
                {
                int LA2_2 = input.LA(2);

                if ( (LA2_2==RULE_ID) ) {
                    int LA2_4 = input.LA(3);

                    if ( (LA2_4==17) ) {
                        alt2=2;
                    }
                    else if ( (LA2_4==18) ) {
                        alt2=3;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 2, 4, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 2, input);

                    throw nvae;
                }
                }
                break;
            case RULE_ID:
            case 20:
            case 22:
                {
                alt2=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:120:5: this_Declaration_de_fonction_0= ruleDeclaration_de_fonction
                    {
                     
                            newCompositeNode(grammarAccess.getAbstractElementAccess().getDeclaration_de_fonctionParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleDeclaration_de_fonction_in_ruleAbstractElement223);
                    this_Declaration_de_fonction_0=ruleDeclaration_de_fonction();

                    state._fsp--;

                     
                            current = this_Declaration_de_fonction_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:130:5: this_Type_1= ruleType
                    {
                     
                            newCompositeNode(grammarAccess.getAbstractElementAccess().getTypeParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleType_in_ruleAbstractElement250);
                    this_Type_1=ruleType();

                    state._fsp--;

                     
                            current = this_Type_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:140:5: this_Instructions_2= ruleInstructions
                    {
                     
                            newCompositeNode(grammarAccess.getAbstractElementAccess().getInstructionsParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_ruleInstructions_in_ruleAbstractElement277);
                    this_Instructions_2=ruleInstructions();

                    state._fsp--;

                     
                            current = this_Instructions_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAbstractElement"


    // $ANTLR start "entryRuleDeclaration_de_fonction"
    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:156:1: entryRuleDeclaration_de_fonction returns [EObject current=null] : iv_ruleDeclaration_de_fonction= ruleDeclaration_de_fonction EOF ;
    public final EObject entryRuleDeclaration_de_fonction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDeclaration_de_fonction = null;


        try {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:157:2: (iv_ruleDeclaration_de_fonction= ruleDeclaration_de_fonction EOF )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:158:2: iv_ruleDeclaration_de_fonction= ruleDeclaration_de_fonction EOF
            {
             newCompositeNode(grammarAccess.getDeclaration_de_fonctionRule()); 
            pushFollow(FOLLOW_ruleDeclaration_de_fonction_in_entryRuleDeclaration_de_fonction312);
            iv_ruleDeclaration_de_fonction=ruleDeclaration_de_fonction();

            state._fsp--;

             current =iv_ruleDeclaration_de_fonction; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDeclaration_de_fonction322); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDeclaration_de_fonction"


    // $ANTLR start "ruleDeclaration_de_fonction"
    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:165:1: ruleDeclaration_de_fonction returns [EObject current=null] : (otherlv_0= 'function' ( (lv_functionName_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_arguments_3_0= RULE_ID ) )? (otherlv_4= ',' ( (lv_argument_5_0= RULE_ID ) ) )* otherlv_6= '){' ( (lv_element_7_0= rulefunctionContent ) )+ otherlv_8= '}' ) ;
    public final EObject ruleDeclaration_de_fonction() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_functionName_1_0=null;
        Token otherlv_2=null;
        Token lv_arguments_3_0=null;
        Token otherlv_4=null;
        Token lv_argument_5_0=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        EObject lv_element_7_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:168:28: ( (otherlv_0= 'function' ( (lv_functionName_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_arguments_3_0= RULE_ID ) )? (otherlv_4= ',' ( (lv_argument_5_0= RULE_ID ) ) )* otherlv_6= '){' ( (lv_element_7_0= rulefunctionContent ) )+ otherlv_8= '}' ) )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:169:1: (otherlv_0= 'function' ( (lv_functionName_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_arguments_3_0= RULE_ID ) )? (otherlv_4= ',' ( (lv_argument_5_0= RULE_ID ) ) )* otherlv_6= '){' ( (lv_element_7_0= rulefunctionContent ) )+ otherlv_8= '}' )
            {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:169:1: (otherlv_0= 'function' ( (lv_functionName_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_arguments_3_0= RULE_ID ) )? (otherlv_4= ',' ( (lv_argument_5_0= RULE_ID ) ) )* otherlv_6= '){' ( (lv_element_7_0= rulefunctionContent ) )+ otherlv_8= '}' )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:169:3: otherlv_0= 'function' ( (lv_functionName_1_0= RULE_ID ) ) otherlv_2= '(' ( (lv_arguments_3_0= RULE_ID ) )? (otherlv_4= ',' ( (lv_argument_5_0= RULE_ID ) ) )* otherlv_6= '){' ( (lv_element_7_0= rulefunctionContent ) )+ otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_11_in_ruleDeclaration_de_fonction359); 

                	newLeafNode(otherlv_0, grammarAccess.getDeclaration_de_fonctionAccess().getFunctionKeyword_0());
                
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:173:1: ( (lv_functionName_1_0= RULE_ID ) )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:174:1: (lv_functionName_1_0= RULE_ID )
            {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:174:1: (lv_functionName_1_0= RULE_ID )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:175:3: lv_functionName_1_0= RULE_ID
            {
            lv_functionName_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleDeclaration_de_fonction376); 

            			newLeafNode(lv_functionName_1_0, grammarAccess.getDeclaration_de_fonctionAccess().getFunctionNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getDeclaration_de_fonctionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"functionName",
                    		lv_functionName_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,12,FOLLOW_12_in_ruleDeclaration_de_fonction393); 

                	newLeafNode(otherlv_2, grammarAccess.getDeclaration_de_fonctionAccess().getLeftParenthesisKeyword_2());
                
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:195:1: ( (lv_arguments_3_0= RULE_ID ) )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_ID) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:196:1: (lv_arguments_3_0= RULE_ID )
                    {
                    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:196:1: (lv_arguments_3_0= RULE_ID )
                    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:197:3: lv_arguments_3_0= RULE_ID
                    {
                    lv_arguments_3_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleDeclaration_de_fonction410); 

                    			newLeafNode(lv_arguments_3_0, grammarAccess.getDeclaration_de_fonctionAccess().getArgumentsIDTerminalRuleCall_3_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getDeclaration_de_fonctionRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"arguments",
                            		lv_arguments_3_0, 
                            		"ID");
                    	    

                    }


                    }
                    break;

            }

            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:213:3: (otherlv_4= ',' ( (lv_argument_5_0= RULE_ID ) ) )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==13) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:213:5: otherlv_4= ',' ( (lv_argument_5_0= RULE_ID ) )
            	    {
            	    otherlv_4=(Token)match(input,13,FOLLOW_13_in_ruleDeclaration_de_fonction429); 

            	        	newLeafNode(otherlv_4, grammarAccess.getDeclaration_de_fonctionAccess().getCommaKeyword_4_0());
            	        
            	    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:217:1: ( (lv_argument_5_0= RULE_ID ) )
            	    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:218:1: (lv_argument_5_0= RULE_ID )
            	    {
            	    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:218:1: (lv_argument_5_0= RULE_ID )
            	    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:219:3: lv_argument_5_0= RULE_ID
            	    {
            	    lv_argument_5_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleDeclaration_de_fonction446); 

            	    			newLeafNode(lv_argument_5_0, grammarAccess.getDeclaration_de_fonctionAccess().getArgumentIDTerminalRuleCall_4_1_0()); 
            	    		

            	    	        if (current==null) {
            	    	            current = createModelElement(grammarAccess.getDeclaration_de_fonctionRule());
            	    	        }
            	           		addWithLastConsumed(
            	           			current, 
            	           			"argument",
            	            		lv_argument_5_0, 
            	            		"ID");
            	    	    

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            otherlv_6=(Token)match(input,14,FOLLOW_14_in_ruleDeclaration_de_fonction465); 

                	newLeafNode(otherlv_6, grammarAccess.getDeclaration_de_fonctionAccess().getRightParenthesisLeftCurlyBracketKeyword_5());
                
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:239:1: ( (lv_element_7_0= rulefunctionContent ) )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==RULE_ID||LA5_0==16||LA5_0==20||LA5_0==22) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:240:1: (lv_element_7_0= rulefunctionContent )
            	    {
            	    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:240:1: (lv_element_7_0= rulefunctionContent )
            	    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:241:3: lv_element_7_0= rulefunctionContent
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getDeclaration_de_fonctionAccess().getElementFunctionContentParserRuleCall_6_0()); 
            	    	    
            	    pushFollow(FOLLOW_rulefunctionContent_in_ruleDeclaration_de_fonction486);
            	    lv_element_7_0=rulefunctionContent();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getDeclaration_de_fonctionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"element",
            	            		lv_element_7_0, 
            	            		"functionContent");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);

            otherlv_8=(Token)match(input,15,FOLLOW_15_in_ruleDeclaration_de_fonction499); 

                	newLeafNode(otherlv_8, grammarAccess.getDeclaration_de_fonctionAccess().getRightCurlyBracketKeyword_7());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDeclaration_de_fonction"


    // $ANTLR start "entryRulefunctionContent"
    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:269:1: entryRulefunctionContent returns [EObject current=null] : iv_rulefunctionContent= rulefunctionContent EOF ;
    public final EObject entryRulefunctionContent() throws RecognitionException {
        EObject current = null;

        EObject iv_rulefunctionContent = null;


        try {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:270:2: (iv_rulefunctionContent= rulefunctionContent EOF )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:271:2: iv_rulefunctionContent= rulefunctionContent EOF
            {
             newCompositeNode(grammarAccess.getFunctionContentRule()); 
            pushFollow(FOLLOW_rulefunctionContent_in_entryRulefunctionContent535);
            iv_rulefunctionContent=rulefunctionContent();

            state._fsp--;

             current =iv_rulefunctionContent; 
            match(input,EOF,FOLLOW_EOF_in_entryRulefunctionContent545); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulefunctionContent"


    // $ANTLR start "rulefunctionContent"
    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:278:1: rulefunctionContent returns [EObject current=null] : (this_Instructions_0= ruleInstructions | this_Type_1= ruleType ) ;
    public final EObject rulefunctionContent() throws RecognitionException {
        EObject current = null;

        EObject this_Instructions_0 = null;

        EObject this_Type_1 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:281:28: ( (this_Instructions_0= ruleInstructions | this_Type_1= ruleType ) )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:282:1: (this_Instructions_0= ruleInstructions | this_Type_1= ruleType )
            {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:282:1: (this_Instructions_0= ruleInstructions | this_Type_1= ruleType )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==16) ) {
                int LA6_1 = input.LA(2);

                if ( (LA6_1==RULE_ID) ) {
                    int LA6_3 = input.LA(3);

                    if ( (LA6_3==17) ) {
                        alt6=2;
                    }
                    else if ( (LA6_3==18) ) {
                        alt6=1;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 6, 3, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 6, 1, input);

                    throw nvae;
                }
            }
            else if ( (LA6_0==RULE_ID||LA6_0==20||LA6_0==22) ) {
                alt6=1;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:283:5: this_Instructions_0= ruleInstructions
                    {
                     
                            newCompositeNode(grammarAccess.getFunctionContentAccess().getInstructionsParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleInstructions_in_rulefunctionContent592);
                    this_Instructions_0=ruleInstructions();

                    state._fsp--;

                     
                            current = this_Instructions_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:293:5: this_Type_1= ruleType
                    {
                     
                            newCompositeNode(grammarAccess.getFunctionContentAccess().getTypeParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleType_in_rulefunctionContent619);
                    this_Type_1=ruleType();

                    state._fsp--;

                     
                            current = this_Type_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulefunctionContent"


    // $ANTLR start "entryRuleType"
    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:309:1: entryRuleType returns [EObject current=null] : iv_ruleType= ruleType EOF ;
    public final EObject entryRuleType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleType = null;


        try {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:310:2: (iv_ruleType= ruleType EOF )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:311:2: iv_ruleType= ruleType EOF
            {
             newCompositeNode(grammarAccess.getTypeRule()); 
            pushFollow(FOLLOW_ruleType_in_entryRuleType654);
            iv_ruleType=ruleType();

            state._fsp--;

             current =iv_ruleType; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleType664); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:318:1: ruleType returns [EObject current=null] : (otherlv_0= 'var' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ';' ) ;
    public final EObject ruleType() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:321:28: ( (otherlv_0= 'var' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ';' ) )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:322:1: (otherlv_0= 'var' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ';' )
            {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:322:1: (otherlv_0= 'var' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ';' )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:322:3: otherlv_0= 'var' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= ';'
            {
            otherlv_0=(Token)match(input,16,FOLLOW_16_in_ruleType701); 

                	newLeafNode(otherlv_0, grammarAccess.getTypeAccess().getVarKeyword_0());
                
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:326:1: ( (lv_name_1_0= RULE_ID ) )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:327:1: (lv_name_1_0= RULE_ID )
            {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:327:1: (lv_name_1_0= RULE_ID )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:328:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleType718); 

            			newLeafNode(lv_name_1_0, grammarAccess.getTypeAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getTypeRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,17,FOLLOW_17_in_ruleType735); 

                	newLeafNode(otherlv_2, grammarAccess.getTypeAccess().getSemicolonKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleInstructions"
    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:356:1: entryRuleInstructions returns [EObject current=null] : iv_ruleInstructions= ruleInstructions EOF ;
    public final EObject entryRuleInstructions() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInstructions = null;


        try {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:357:2: (iv_ruleInstructions= ruleInstructions EOF )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:358:2: iv_ruleInstructions= ruleInstructions EOF
            {
             newCompositeNode(grammarAccess.getInstructionsRule()); 
            pushFollow(FOLLOW_ruleInstructions_in_entryRuleInstructions771);
            iv_ruleInstructions=ruleInstructions();

            state._fsp--;

             current =iv_ruleInstructions; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInstructions781); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInstructions"


    // $ANTLR start "ruleInstructions"
    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:365:1: ruleInstructions returns [EObject current=null] : (this_instanciation_0= ruleinstanciation | this_addition_1= ruleaddition | this_alert_2= rulealert | this_prompt_3= ruleprompt | ( ( (lv_name_4_0= RULE_ID ) ) otherlv_5= ';' ) ) ;
    public final EObject ruleInstructions() throws RecognitionException {
        EObject current = null;

        Token lv_name_4_0=null;
        Token otherlv_5=null;
        EObject this_instanciation_0 = null;

        EObject this_addition_1 = null;

        EObject this_alert_2 = null;

        EObject this_prompt_3 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:368:28: ( (this_instanciation_0= ruleinstanciation | this_addition_1= ruleaddition | this_alert_2= rulealert | this_prompt_3= ruleprompt | ( ( (lv_name_4_0= RULE_ID ) ) otherlv_5= ';' ) ) )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:369:1: (this_instanciation_0= ruleinstanciation | this_addition_1= ruleaddition | this_alert_2= rulealert | this_prompt_3= ruleprompt | ( ( (lv_name_4_0= RULE_ID ) ) otherlv_5= ';' ) )
            {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:369:1: (this_instanciation_0= ruleinstanciation | this_addition_1= ruleaddition | this_alert_2= rulealert | this_prompt_3= ruleprompt | ( ( (lv_name_4_0= RULE_ID ) ) otherlv_5= ';' ) )
            int alt7=5;
            alt7 = dfa7.predict(input);
            switch (alt7) {
                case 1 :
                    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:370:5: this_instanciation_0= ruleinstanciation
                    {
                     
                            newCompositeNode(grammarAccess.getInstructionsAccess().getInstanciationParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleinstanciation_in_ruleInstructions828);
                    this_instanciation_0=ruleinstanciation();

                    state._fsp--;

                     
                            current = this_instanciation_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:380:5: this_addition_1= ruleaddition
                    {
                     
                            newCompositeNode(grammarAccess.getInstructionsAccess().getAdditionParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleaddition_in_ruleInstructions855);
                    this_addition_1=ruleaddition();

                    state._fsp--;

                     
                            current = this_addition_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:390:5: this_alert_2= rulealert
                    {
                     
                            newCompositeNode(grammarAccess.getInstructionsAccess().getAlertParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_rulealert_in_ruleInstructions882);
                    this_alert_2=rulealert();

                    state._fsp--;

                     
                            current = this_alert_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:400:5: this_prompt_3= ruleprompt
                    {
                     
                            newCompositeNode(grammarAccess.getInstructionsAccess().getPromptParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_ruleprompt_in_ruleInstructions909);
                    this_prompt_3=ruleprompt();

                    state._fsp--;

                     
                            current = this_prompt_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 5 :
                    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:409:6: ( ( (lv_name_4_0= RULE_ID ) ) otherlv_5= ';' )
                    {
                    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:409:6: ( ( (lv_name_4_0= RULE_ID ) ) otherlv_5= ';' )
                    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:409:7: ( (lv_name_4_0= RULE_ID ) ) otherlv_5= ';'
                    {
                    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:409:7: ( (lv_name_4_0= RULE_ID ) )
                    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:410:1: (lv_name_4_0= RULE_ID )
                    {
                    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:410:1: (lv_name_4_0= RULE_ID )
                    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:411:3: lv_name_4_0= RULE_ID
                    {
                    lv_name_4_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleInstructions932); 

                    			newLeafNode(lv_name_4_0, grammarAccess.getInstructionsAccess().getNameIDTerminalRuleCall_4_0_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getInstructionsRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"name",
                            		lv_name_4_0, 
                            		"ID");
                    	    

                    }


                    }

                    otherlv_5=(Token)match(input,17,FOLLOW_17_in_ruleInstructions949); 

                        	newLeafNode(otherlv_5, grammarAccess.getInstructionsAccess().getSemicolonKeyword_4_1());
                        

                    }


                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInstructions"


    // $ANTLR start "entryRuleinstanciation"
    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:439:1: entryRuleinstanciation returns [EObject current=null] : iv_ruleinstanciation= ruleinstanciation EOF ;
    public final EObject entryRuleinstanciation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleinstanciation = null;


        try {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:440:2: (iv_ruleinstanciation= ruleinstanciation EOF )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:441:2: iv_ruleinstanciation= ruleinstanciation EOF
            {
             newCompositeNode(grammarAccess.getInstanciationRule()); 
            pushFollow(FOLLOW_ruleinstanciation_in_entryRuleinstanciation986);
            iv_ruleinstanciation=ruleinstanciation();

            state._fsp--;

             current =iv_ruleinstanciation; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleinstanciation996); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleinstanciation"


    // $ANTLR start "ruleinstanciation"
    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:448:1: ruleinstanciation returns [EObject current=null] : ( (otherlv_0= 'var' )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_variable_3_0= RULE_ID ) ) otherlv_4= ';' ) ;
    public final EObject ruleinstanciation() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_variable_3_0=null;
        Token otherlv_4=null;

         enterRule(); 
            
        try {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:451:28: ( ( (otherlv_0= 'var' )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_variable_3_0= RULE_ID ) ) otherlv_4= ';' ) )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:452:1: ( (otherlv_0= 'var' )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_variable_3_0= RULE_ID ) ) otherlv_4= ';' )
            {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:452:1: ( (otherlv_0= 'var' )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_variable_3_0= RULE_ID ) ) otherlv_4= ';' )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:452:2: (otherlv_0= 'var' )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_variable_3_0= RULE_ID ) ) otherlv_4= ';'
            {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:452:2: (otherlv_0= 'var' )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==16) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:452:4: otherlv_0= 'var'
                    {
                    otherlv_0=(Token)match(input,16,FOLLOW_16_in_ruleinstanciation1034); 

                        	newLeafNode(otherlv_0, grammarAccess.getInstanciationAccess().getVarKeyword_0());
                        

                    }
                    break;

            }

            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:456:3: ( (lv_name_1_0= RULE_ID ) )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:457:1: (lv_name_1_0= RULE_ID )
            {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:457:1: (lv_name_1_0= RULE_ID )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:458:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleinstanciation1053); 

            			newLeafNode(lv_name_1_0, grammarAccess.getInstanciationAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getInstanciationRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,18,FOLLOW_18_in_ruleinstanciation1070); 

                	newLeafNode(otherlv_2, grammarAccess.getInstanciationAccess().getEqualsSignKeyword_2());
                
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:478:1: ( (lv_variable_3_0= RULE_ID ) )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:479:1: (lv_variable_3_0= RULE_ID )
            {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:479:1: (lv_variable_3_0= RULE_ID )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:480:3: lv_variable_3_0= RULE_ID
            {
            lv_variable_3_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleinstanciation1087); 

            			newLeafNode(lv_variable_3_0, grammarAccess.getInstanciationAccess().getVariableIDTerminalRuleCall_3_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getInstanciationRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"variable",
                    		lv_variable_3_0, 
                    		"ID");
            	    

            }


            }

            otherlv_4=(Token)match(input,17,FOLLOW_17_in_ruleinstanciation1104); 

                	newLeafNode(otherlv_4, grammarAccess.getInstanciationAccess().getSemicolonKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleinstanciation"


    // $ANTLR start "entryRuleaddition"
    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:508:1: entryRuleaddition returns [EObject current=null] : iv_ruleaddition= ruleaddition EOF ;
    public final EObject entryRuleaddition() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleaddition = null;


        try {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:509:2: (iv_ruleaddition= ruleaddition EOF )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:510:2: iv_ruleaddition= ruleaddition EOF
            {
             newCompositeNode(grammarAccess.getAdditionRule()); 
            pushFollow(FOLLOW_ruleaddition_in_entryRuleaddition1140);
            iv_ruleaddition=ruleaddition();

            state._fsp--;

             current =iv_ruleaddition; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleaddition1150); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleaddition"


    // $ANTLR start "ruleaddition"
    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:517:1: ruleaddition returns [EObject current=null] : ( (otherlv_0= 'var' )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_variable1_3_0= RULE_ID ) ) otherlv_4= '+' ( (lv_variable2_5_0= RULE_ID ) ) otherlv_6= ';' ) ;
    public final EObject ruleaddition() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token lv_variable1_3_0=null;
        Token otherlv_4=null;
        Token lv_variable2_5_0=null;
        Token otherlv_6=null;

         enterRule(); 
            
        try {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:520:28: ( ( (otherlv_0= 'var' )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_variable1_3_0= RULE_ID ) ) otherlv_4= '+' ( (lv_variable2_5_0= RULE_ID ) ) otherlv_6= ';' ) )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:521:1: ( (otherlv_0= 'var' )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_variable1_3_0= RULE_ID ) ) otherlv_4= '+' ( (lv_variable2_5_0= RULE_ID ) ) otherlv_6= ';' )
            {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:521:1: ( (otherlv_0= 'var' )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_variable1_3_0= RULE_ID ) ) otherlv_4= '+' ( (lv_variable2_5_0= RULE_ID ) ) otherlv_6= ';' )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:521:2: (otherlv_0= 'var' )? ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_variable1_3_0= RULE_ID ) ) otherlv_4= '+' ( (lv_variable2_5_0= RULE_ID ) ) otherlv_6= ';'
            {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:521:2: (otherlv_0= 'var' )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==16) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:521:4: otherlv_0= 'var'
                    {
                    otherlv_0=(Token)match(input,16,FOLLOW_16_in_ruleaddition1188); 

                        	newLeafNode(otherlv_0, grammarAccess.getAdditionAccess().getVarKeyword_0());
                        

                    }
                    break;

            }

            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:525:3: ( (lv_name_1_0= RULE_ID ) )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:526:1: (lv_name_1_0= RULE_ID )
            {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:526:1: (lv_name_1_0= RULE_ID )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:527:3: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleaddition1207); 

            			newLeafNode(lv_name_1_0, grammarAccess.getAdditionAccess().getNameIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getAdditionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"name",
                    		lv_name_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,18,FOLLOW_18_in_ruleaddition1224); 

                	newLeafNode(otherlv_2, grammarAccess.getAdditionAccess().getEqualsSignKeyword_2());
                
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:547:1: ( (lv_variable1_3_0= RULE_ID ) )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:548:1: (lv_variable1_3_0= RULE_ID )
            {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:548:1: (lv_variable1_3_0= RULE_ID )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:549:3: lv_variable1_3_0= RULE_ID
            {
            lv_variable1_3_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleaddition1241); 

            			newLeafNode(lv_variable1_3_0, grammarAccess.getAdditionAccess().getVariable1IDTerminalRuleCall_3_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getAdditionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"variable1",
                    		lv_variable1_3_0, 
                    		"ID");
            	    

            }


            }

            otherlv_4=(Token)match(input,19,FOLLOW_19_in_ruleaddition1258); 

                	newLeafNode(otherlv_4, grammarAccess.getAdditionAccess().getPlusSignKeyword_4());
                
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:569:1: ( (lv_variable2_5_0= RULE_ID ) )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:570:1: (lv_variable2_5_0= RULE_ID )
            {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:570:1: (lv_variable2_5_0= RULE_ID )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:571:3: lv_variable2_5_0= RULE_ID
            {
            lv_variable2_5_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleaddition1275); 

            			newLeafNode(lv_variable2_5_0, grammarAccess.getAdditionAccess().getVariable2IDTerminalRuleCall_5_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getAdditionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"variable2",
                    		lv_variable2_5_0, 
                    		"ID");
            	    

            }


            }

            otherlv_6=(Token)match(input,17,FOLLOW_17_in_ruleaddition1292); 

                	newLeafNode(otherlv_6, grammarAccess.getAdditionAccess().getSemicolonKeyword_6());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleaddition"


    // $ANTLR start "entryRulealert"
    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:599:1: entryRulealert returns [EObject current=null] : iv_rulealert= rulealert EOF ;
    public final EObject entryRulealert() throws RecognitionException {
        EObject current = null;

        EObject iv_rulealert = null;


        try {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:600:2: (iv_rulealert= rulealert EOF )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:601:2: iv_rulealert= rulealert EOF
            {
             newCompositeNode(grammarAccess.getAlertRule()); 
            pushFollow(FOLLOW_rulealert_in_entryRulealert1328);
            iv_rulealert=rulealert();

            state._fsp--;

             current =iv_rulealert; 
            match(input,EOF,FOLLOW_EOF_in_entryRulealert1338); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulealert"


    // $ANTLR start "rulealert"
    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:608:1: rulealert returns [EObject current=null] : (otherlv_0= 'alert(\"' ( (lv_message_1_0= RULE_ID ) ) otherlv_2= '\");' ) ;
    public final EObject rulealert() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_message_1_0=null;
        Token otherlv_2=null;

         enterRule(); 
            
        try {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:611:28: ( (otherlv_0= 'alert(\"' ( (lv_message_1_0= RULE_ID ) ) otherlv_2= '\");' ) )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:612:1: (otherlv_0= 'alert(\"' ( (lv_message_1_0= RULE_ID ) ) otherlv_2= '\");' )
            {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:612:1: (otherlv_0= 'alert(\"' ( (lv_message_1_0= RULE_ID ) ) otherlv_2= '\");' )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:612:3: otherlv_0= 'alert(\"' ( (lv_message_1_0= RULE_ID ) ) otherlv_2= '\");'
            {
            otherlv_0=(Token)match(input,20,FOLLOW_20_in_rulealert1375); 

                	newLeafNode(otherlv_0, grammarAccess.getAlertAccess().getAlertKeyword_0());
                
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:616:1: ( (lv_message_1_0= RULE_ID ) )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:617:1: (lv_message_1_0= RULE_ID )
            {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:617:1: (lv_message_1_0= RULE_ID )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:618:3: lv_message_1_0= RULE_ID
            {
            lv_message_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_rulealert1392); 

            			newLeafNode(lv_message_1_0, grammarAccess.getAlertAccess().getMessageIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getAlertRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"message",
                    		lv_message_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,21,FOLLOW_21_in_rulealert1409); 

                	newLeafNode(otherlv_2, grammarAccess.getAlertAccess().getQuotationMarkRightParenthesisSemicolonKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulealert"


    // $ANTLR start "entryRuleprompt"
    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:646:1: entryRuleprompt returns [EObject current=null] : iv_ruleprompt= ruleprompt EOF ;
    public final EObject entryRuleprompt() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleprompt = null;


        try {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:647:2: (iv_ruleprompt= ruleprompt EOF )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:648:2: iv_ruleprompt= ruleprompt EOF
            {
             newCompositeNode(grammarAccess.getPromptRule()); 
            pushFollow(FOLLOW_ruleprompt_in_entryRuleprompt1445);
            iv_ruleprompt=ruleprompt();

            state._fsp--;

             current =iv_ruleprompt; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleprompt1455); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleprompt"


    // $ANTLR start "ruleprompt"
    // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:655:1: ruleprompt returns [EObject current=null] : (otherlv_0= 'prompt(\"' ( (lv_votre_text_1_0= RULE_ID ) ) otherlv_2= ',' ( (lv_text_par_defaut_3_0= RULE_ID ) ) otherlv_4= '\");' ) ;
    public final EObject ruleprompt() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_votre_text_1_0=null;
        Token otherlv_2=null;
        Token lv_text_par_defaut_3_0=null;
        Token otherlv_4=null;

         enterRule(); 
            
        try {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:658:28: ( (otherlv_0= 'prompt(\"' ( (lv_votre_text_1_0= RULE_ID ) ) otherlv_2= ',' ( (lv_text_par_defaut_3_0= RULE_ID ) ) otherlv_4= '\");' ) )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:659:1: (otherlv_0= 'prompt(\"' ( (lv_votre_text_1_0= RULE_ID ) ) otherlv_2= ',' ( (lv_text_par_defaut_3_0= RULE_ID ) ) otherlv_4= '\");' )
            {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:659:1: (otherlv_0= 'prompt(\"' ( (lv_votre_text_1_0= RULE_ID ) ) otherlv_2= ',' ( (lv_text_par_defaut_3_0= RULE_ID ) ) otherlv_4= '\");' )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:659:3: otherlv_0= 'prompt(\"' ( (lv_votre_text_1_0= RULE_ID ) ) otherlv_2= ',' ( (lv_text_par_defaut_3_0= RULE_ID ) ) otherlv_4= '\");'
            {
            otherlv_0=(Token)match(input,22,FOLLOW_22_in_ruleprompt1492); 

                	newLeafNode(otherlv_0, grammarAccess.getPromptAccess().getPromptKeyword_0());
                
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:663:1: ( (lv_votre_text_1_0= RULE_ID ) )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:664:1: (lv_votre_text_1_0= RULE_ID )
            {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:664:1: (lv_votre_text_1_0= RULE_ID )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:665:3: lv_votre_text_1_0= RULE_ID
            {
            lv_votre_text_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleprompt1509); 

            			newLeafNode(lv_votre_text_1_0, grammarAccess.getPromptAccess().getVotre_textIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getPromptRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"votre_text",
                    		lv_votre_text_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,13,FOLLOW_13_in_ruleprompt1526); 

                	newLeafNode(otherlv_2, grammarAccess.getPromptAccess().getCommaKeyword_2());
                
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:685:1: ( (lv_text_par_defaut_3_0= RULE_ID ) )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:686:1: (lv_text_par_defaut_3_0= RULE_ID )
            {
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:686:1: (lv_text_par_defaut_3_0= RULE_ID )
            // ../org.xtext.editorforjs/src-gen/org/xtext/parser/antlr/internal/InternalEditorForJS.g:687:3: lv_text_par_defaut_3_0= RULE_ID
            {
            lv_text_par_defaut_3_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleprompt1543); 

            			newLeafNode(lv_text_par_defaut_3_0, grammarAccess.getPromptAccess().getText_par_defautIDTerminalRuleCall_3_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getPromptRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"text_par_defaut",
                    		lv_text_par_defaut_3_0, 
                    		"ID");
            	    

            }


            }

            otherlv_4=(Token)match(input,21,FOLLOW_21_in_ruleprompt1560); 

                	newLeafNode(otherlv_4, grammarAccess.getPromptAccess().getQuotationMarkRightParenthesisSemicolonKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleprompt"

    // Delegated rules


    protected DFA7 dfa7 = new DFA7(this);
    static final String DFA7_eotS =
        "\13\uffff";
    static final String DFA7_eofS =
        "\13\uffff";
    static final String DFA7_minS =
        "\2\4\1\21\2\uffff\1\22\1\4\1\uffff\1\21\2\uffff";
    static final String DFA7_maxS =
        "\1\26\1\4\1\22\2\uffff\1\22\1\4\1\uffff\1\23\2\uffff";
    static final String DFA7_acceptS =
        "\3\uffff\1\3\1\4\2\uffff\1\5\1\uffff\1\1\1\2";
    static final String DFA7_specialS =
        "\13\uffff}>";
    static final String[] DFA7_transitionS = {
            "\1\2\13\uffff\1\1\3\uffff\1\3\1\uffff\1\4",
            "\1\5",
            "\1\7\1\6",
            "",
            "",
            "\1\6",
            "\1\10",
            "",
            "\1\11\1\uffff\1\12",
            "",
            ""
    };

    static final short[] DFA7_eot = DFA.unpackEncodedString(DFA7_eotS);
    static final short[] DFA7_eof = DFA.unpackEncodedString(DFA7_eofS);
    static final char[] DFA7_min = DFA.unpackEncodedStringToUnsignedChars(DFA7_minS);
    static final char[] DFA7_max = DFA.unpackEncodedStringToUnsignedChars(DFA7_maxS);
    static final short[] DFA7_accept = DFA.unpackEncodedString(DFA7_acceptS);
    static final short[] DFA7_special = DFA.unpackEncodedString(DFA7_specialS);
    static final short[][] DFA7_transition;

    static {
        int numStates = DFA7_transitionS.length;
        DFA7_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA7_transition[i] = DFA.unpackEncodedString(DFA7_transitionS[i]);
        }
    }

    class DFA7 extends DFA {

        public DFA7(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 7;
            this.eot = DFA7_eot;
            this.eof = DFA7_eof;
            this.min = DFA7_min;
            this.max = DFA7_max;
            this.accept = DFA7_accept;
            this.special = DFA7_special;
            this.transition = DFA7_transition;
        }
        public String getDescription() {
            return "369:1: (this_instanciation_0= ruleinstanciation | this_addition_1= ruleaddition | this_alert_2= rulealert | this_prompt_3= ruleprompt | ( ( (lv_name_4_0= RULE_ID ) ) otherlv_5= ';' ) )";
        }
    }
 

    public static final BitSet FOLLOW_ruleDomainmodel_in_entryRuleDomainmodel75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDomainmodel85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAbstractElement_in_ruleDomainmodel130 = new BitSet(new long[]{0x0000000000510812L});
    public static final BitSet FOLLOW_ruleAbstractElement_in_entryRuleAbstractElement166 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAbstractElement176 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDeclaration_de_fonction_in_ruleAbstractElement223 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleType_in_ruleAbstractElement250 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInstructions_in_ruleAbstractElement277 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDeclaration_de_fonction_in_entryRuleDeclaration_de_fonction312 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDeclaration_de_fonction322 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_ruleDeclaration_de_fonction359 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleDeclaration_de_fonction376 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleDeclaration_de_fonction393 = new BitSet(new long[]{0x0000000000006010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleDeclaration_de_fonction410 = new BitSet(new long[]{0x0000000000006000L});
    public static final BitSet FOLLOW_13_in_ruleDeclaration_de_fonction429 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleDeclaration_de_fonction446 = new BitSet(new long[]{0x0000000000006000L});
    public static final BitSet FOLLOW_14_in_ruleDeclaration_de_fonction465 = new BitSet(new long[]{0x0000000000510810L});
    public static final BitSet FOLLOW_rulefunctionContent_in_ruleDeclaration_de_fonction486 = new BitSet(new long[]{0x0000000000518810L});
    public static final BitSet FOLLOW_15_in_ruleDeclaration_de_fonction499 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulefunctionContent_in_entryRulefunctionContent535 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulefunctionContent545 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInstructions_in_rulefunctionContent592 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleType_in_rulefunctionContent619 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleType_in_entryRuleType654 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleType664 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_ruleType701 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleType718 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleType735 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInstructions_in_entryRuleInstructions771 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInstructions781 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleinstanciation_in_ruleInstructions828 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleaddition_in_ruleInstructions855 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulealert_in_ruleInstructions882 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleprompt_in_ruleInstructions909 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleInstructions932 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleInstructions949 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleinstanciation_in_entryRuleinstanciation986 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleinstanciation996 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_ruleinstanciation1034 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleinstanciation1053 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_ruleinstanciation1070 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleinstanciation1087 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleinstanciation1104 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleaddition_in_entryRuleaddition1140 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleaddition1150 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_ruleaddition1188 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleaddition1207 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_18_in_ruleaddition1224 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleaddition1241 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_19_in_ruleaddition1258 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleaddition1275 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleaddition1292 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulealert_in_entryRulealert1328 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulealert1338 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rulealert1375 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_rulealert1392 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_rulealert1409 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleprompt_in_entryRuleprompt1445 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleprompt1455 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_ruleprompt1492 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleprompt1509 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_ruleprompt1526 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleprompt1543 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_21_in_ruleprompt1560 = new BitSet(new long[]{0x0000000000000002L});

}