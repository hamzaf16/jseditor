/*
* generated by Xtext
*/
package org.xtext.parser.antlr;

import com.google.inject.Inject;

import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.xtext.services.EditorForJSGrammarAccess;

public class EditorForJSParser extends org.eclipse.xtext.parser.antlr.AbstractAntlrParser {
	
	@Inject
	private EditorForJSGrammarAccess grammarAccess;
	
	@Override
	protected void setInitialHiddenTokens(XtextTokenStream tokenStream) {
		tokenStream.setInitialHiddenTokens("RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT");
	}
	
	@Override
	protected org.xtext.parser.antlr.internal.InternalEditorForJSParser createParser(XtextTokenStream stream) {
		return new org.xtext.parser.antlr.internal.InternalEditorForJSParser(stream, getGrammarAccess());
	}
	
	@Override 
	protected String getDefaultRuleName() {
		return "Domainmodel";
	}
	
	public EditorForJSGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}
	
	public void setGrammarAccess(EditorForJSGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
	
}
