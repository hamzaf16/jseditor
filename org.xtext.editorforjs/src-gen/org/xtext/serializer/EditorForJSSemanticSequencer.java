package org.xtext.serializer;

import com.google.inject.Inject;
import com.google.inject.Provider;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.serializer.acceptor.ISemanticSequenceAcceptor;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.diagnostic.ISemanticSequencerDiagnosticProvider;
import org.eclipse.xtext.serializer.diagnostic.ISerializationDiagnostic.Acceptor;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.GenericSequencer;
import org.eclipse.xtext.serializer.sequencer.ISemanticNodeProvider.INodesForEObjectProvider;
import org.eclipse.xtext.serializer.sequencer.ISemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;
import org.xtext.editorForJS.Declaration_de_fonction;
import org.xtext.editorForJS.Domainmodel;
import org.xtext.editorForJS.EditorForJSPackage;
import org.xtext.editorForJS.Instructions;
import org.xtext.editorForJS.Type;
import org.xtext.editorForJS.addition;
import org.xtext.editorForJS.alert;
import org.xtext.editorForJS.instanciation;
import org.xtext.editorForJS.prompt;
import org.xtext.services.EditorForJSGrammarAccess;

@SuppressWarnings("all")
public class EditorForJSSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private EditorForJSGrammarAccess grammarAccess;
	
	public void createSequence(EObject context, EObject semanticObject) {
		if(semanticObject.eClass().getEPackage() == EditorForJSPackage.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case EditorForJSPackage.DECLARATION_DE_FONCTION:
				if(context == grammarAccess.getAbstractElementRule() ||
				   context == grammarAccess.getDeclaration_de_fonctionRule()) {
					sequence_Declaration_de_fonction(context, (Declaration_de_fonction) semanticObject); 
					return; 
				}
				else break;
			case EditorForJSPackage.DOMAINMODEL:
				if(context == grammarAccess.getDomainmodelRule()) {
					sequence_Domainmodel(context, (Domainmodel) semanticObject); 
					return; 
				}
				else break;
			case EditorForJSPackage.INSTRUCTIONS:
				if(context == grammarAccess.getAbstractElementRule() ||
				   context == grammarAccess.getInstructionsRule() ||
				   context == grammarAccess.getFunctionContentRule()) {
					sequence_Instructions(context, (Instructions) semanticObject); 
					return; 
				}
				else break;
			case EditorForJSPackage.TYPE:
				if(context == grammarAccess.getAbstractElementRule() ||
				   context == grammarAccess.getTypeRule() ||
				   context == grammarAccess.getFunctionContentRule()) {
					sequence_Type(context, (Type) semanticObject); 
					return; 
				}
				else break;
			case EditorForJSPackage.ADDITION:
				if(context == grammarAccess.getAbstractElementRule() ||
				   context == grammarAccess.getInstructionsRule() ||
				   context == grammarAccess.getAdditionRule() ||
				   context == grammarAccess.getFunctionContentRule()) {
					sequence_addition(context, (addition) semanticObject); 
					return; 
				}
				else break;
			case EditorForJSPackage.ALERT:
				if(context == grammarAccess.getAbstractElementRule() ||
				   context == grammarAccess.getInstructionsRule() ||
				   context == grammarAccess.getAlertRule() ||
				   context == grammarAccess.getFunctionContentRule()) {
					sequence_alert(context, (alert) semanticObject); 
					return; 
				}
				else break;
			case EditorForJSPackage.INSTANCIATION:
				if(context == grammarAccess.getAbstractElementRule() ||
				   context == grammarAccess.getInstructionsRule() ||
				   context == grammarAccess.getFunctionContentRule() ||
				   context == grammarAccess.getInstanciationRule()) {
					sequence_instanciation(context, (instanciation) semanticObject); 
					return; 
				}
				else break;
			case EditorForJSPackage.PROMPT:
				if(context == grammarAccess.getAbstractElementRule() ||
				   context == grammarAccess.getInstructionsRule() ||
				   context == grammarAccess.getFunctionContentRule() ||
				   context == grammarAccess.getPromptRule()) {
					sequence_prompt(context, (prompt) semanticObject); 
					return; 
				}
				else break;
			}
		if (errorAcceptor != null) errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Constraint:
	 *     (functionName=ID arguments=ID? argument+=ID* element+=functionContent+)
	 */
	protected void sequence_Declaration_de_fonction(EObject context, Declaration_de_fonction semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     elements+=AbstractElement*
	 */
	protected void sequence_Domainmodel(EObject context, Domainmodel semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     name=ID
	 */
	protected void sequence_Instructions(EObject context, Instructions semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, EditorForJSPackage.Literals.INSTRUCTIONS__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, EditorForJSPackage.Literals.INSTRUCTIONS__NAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getInstructionsAccess().getNameIDTerminalRuleCall_4_0_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     name=ID
	 */
	protected void sequence_Type(EObject context, Type semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, EditorForJSPackage.Literals.TYPE__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, EditorForJSPackage.Literals.TYPE__NAME));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getTypeAccess().getNameIDTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID variable1=ID variable2=ID)
	 */
	protected void sequence_addition(EObject context, addition semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, EditorForJSPackage.Literals.INSTRUCTIONS__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, EditorForJSPackage.Literals.INSTRUCTIONS__NAME));
			if(transientValues.isValueTransient(semanticObject, EditorForJSPackage.Literals.ADDITION__VARIABLE1) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, EditorForJSPackage.Literals.ADDITION__VARIABLE1));
			if(transientValues.isValueTransient(semanticObject, EditorForJSPackage.Literals.ADDITION__VARIABLE2) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, EditorForJSPackage.Literals.ADDITION__VARIABLE2));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getAdditionAccess().getNameIDTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getAdditionAccess().getVariable1IDTerminalRuleCall_3_0(), semanticObject.getVariable1());
		feeder.accept(grammarAccess.getAdditionAccess().getVariable2IDTerminalRuleCall_5_0(), semanticObject.getVariable2());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     message=ID
	 */
	protected void sequence_alert(EObject context, alert semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (name=ID variable=ID)
	 */
	protected void sequence_instanciation(EObject context, instanciation semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, EditorForJSPackage.Literals.INSTRUCTIONS__NAME) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, EditorForJSPackage.Literals.INSTRUCTIONS__NAME));
			if(transientValues.isValueTransient(semanticObject, EditorForJSPackage.Literals.INSTANCIATION__VARIABLE) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, EditorForJSPackage.Literals.INSTANCIATION__VARIABLE));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getInstanciationAccess().getNameIDTerminalRuleCall_1_0(), semanticObject.getName());
		feeder.accept(grammarAccess.getInstanciationAccess().getVariableIDTerminalRuleCall_3_0(), semanticObject.getVariable());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (votre_text=ID text_par_defaut=ID)
	 */
	protected void sequence_prompt(EObject context, prompt semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
}
