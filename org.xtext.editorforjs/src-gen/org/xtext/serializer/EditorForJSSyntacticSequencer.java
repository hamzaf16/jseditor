package org.xtext.serializer;

import com.google.inject.Inject;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.IGrammarAccess;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.AbstractElementAlias;
import org.eclipse.xtext.serializer.analysis.GrammarAlias.TokenAlias;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynNavigable;
import org.eclipse.xtext.serializer.analysis.ISyntacticSequencerPDAProvider.ISynTransition;
import org.eclipse.xtext.serializer.sequencer.AbstractSyntacticSequencer;
import org.xtext.services.EditorForJSGrammarAccess;

@SuppressWarnings("all")
public class EditorForJSSyntacticSequencer extends AbstractSyntacticSequencer {

	protected EditorForJSGrammarAccess grammarAccess;
	protected AbstractElementAlias match_addition_VarKeyword_0_q;
	protected AbstractElementAlias match_instanciation_VarKeyword_0_q;
	
	@Inject
	protected void init(IGrammarAccess access) {
		grammarAccess = (EditorForJSGrammarAccess) access;
		match_addition_VarKeyword_0_q = new TokenAlias(false, true, grammarAccess.getAdditionAccess().getVarKeyword_0());
		match_instanciation_VarKeyword_0_q = new TokenAlias(false, true, grammarAccess.getInstanciationAccess().getVarKeyword_0());
	}
	
	@Override
	protected String getUnassignedRuleCallToken(EObject semanticObject, RuleCall ruleCall, INode node) {
		return "";
	}
	
	
	@Override
	protected void emitUnassignedTokens(EObject semanticObject, ISynTransition transition, INode fromNode, INode toNode) {
		if (transition.getAmbiguousSyntaxes().isEmpty()) return;
		List<INode> transitionNodes = collectNodes(fromNode, toNode);
		for (AbstractElementAlias syntax : transition.getAmbiguousSyntaxes()) {
			List<INode> syntaxNodes = getNodesFor(transitionNodes, syntax);
			if(match_addition_VarKeyword_0_q.equals(syntax))
				emit_addition_VarKeyword_0_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else if(match_instanciation_VarKeyword_0_q.equals(syntax))
				emit_instanciation_VarKeyword_0_q(semanticObject, getLastNavigableState(), syntaxNodes);
			else acceptNodes(getLastNavigableState(), syntaxNodes);
		}
	}

	/**
	 * Syntax:
	 *     'var'?
	 */
	protected void emit_addition_VarKeyword_0_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
	/**
	 * Syntax:
	 *     'var'?
	 */
	protected void emit_instanciation_VarKeyword_0_q(EObject semanticObject, ISynNavigable transition, List<INode> nodes) {
		acceptNodes(transition, nodes);
	}
	
}
