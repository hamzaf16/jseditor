package org.xtext.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import org.xtext.services.EditorForJSGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalEditorForJSParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'function'", "'('", "'){'", "'}'", "','", "'var'", "';'", "'='", "'+'", "'alert(\"'", "'\");'", "'prompt(\"'"
    };
    public static final int RULE_ID=4;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__19=19;
    public static final int RULE_STRING=6;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=5;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalEditorForJSParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalEditorForJSParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalEditorForJSParser.tokenNames; }
    public String getGrammarFileName() { return "../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g"; }


     
     	private EditorForJSGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(EditorForJSGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleDomainmodel"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:60:1: entryRuleDomainmodel : ruleDomainmodel EOF ;
    public final void entryRuleDomainmodel() throws RecognitionException {
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:61:1: ( ruleDomainmodel EOF )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:62:1: ruleDomainmodel EOF
            {
             before(grammarAccess.getDomainmodelRule()); 
            pushFollow(FOLLOW_ruleDomainmodel_in_entryRuleDomainmodel61);
            ruleDomainmodel();

            state._fsp--;

             after(grammarAccess.getDomainmodelRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDomainmodel68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDomainmodel"


    // $ANTLR start "ruleDomainmodel"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:69:1: ruleDomainmodel : ( ( rule__Domainmodel__ElementsAssignment )* ) ;
    public final void ruleDomainmodel() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:73:2: ( ( ( rule__Domainmodel__ElementsAssignment )* ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:74:1: ( ( rule__Domainmodel__ElementsAssignment )* )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:74:1: ( ( rule__Domainmodel__ElementsAssignment )* )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:75:1: ( rule__Domainmodel__ElementsAssignment )*
            {
             before(grammarAccess.getDomainmodelAccess().getElementsAssignment()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:76:1: ( rule__Domainmodel__ElementsAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==RULE_ID||LA1_0==11||LA1_0==16||LA1_0==20||LA1_0==22) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:76:2: rule__Domainmodel__ElementsAssignment
            	    {
            	    pushFollow(FOLLOW_rule__Domainmodel__ElementsAssignment_in_ruleDomainmodel94);
            	    rule__Domainmodel__ElementsAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getDomainmodelAccess().getElementsAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDomainmodel"


    // $ANTLR start "entryRuleAbstractElement"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:88:1: entryRuleAbstractElement : ruleAbstractElement EOF ;
    public final void entryRuleAbstractElement() throws RecognitionException {
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:89:1: ( ruleAbstractElement EOF )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:90:1: ruleAbstractElement EOF
            {
             before(grammarAccess.getAbstractElementRule()); 
            pushFollow(FOLLOW_ruleAbstractElement_in_entryRuleAbstractElement122);
            ruleAbstractElement();

            state._fsp--;

             after(grammarAccess.getAbstractElementRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleAbstractElement129); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAbstractElement"


    // $ANTLR start "ruleAbstractElement"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:97:1: ruleAbstractElement : ( ( rule__AbstractElement__Alternatives ) ) ;
    public final void ruleAbstractElement() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:101:2: ( ( ( rule__AbstractElement__Alternatives ) ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:102:1: ( ( rule__AbstractElement__Alternatives ) )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:102:1: ( ( rule__AbstractElement__Alternatives ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:103:1: ( rule__AbstractElement__Alternatives )
            {
             before(grammarAccess.getAbstractElementAccess().getAlternatives()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:104:1: ( rule__AbstractElement__Alternatives )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:104:2: rule__AbstractElement__Alternatives
            {
            pushFollow(FOLLOW_rule__AbstractElement__Alternatives_in_ruleAbstractElement155);
            rule__AbstractElement__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAbstractElementAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAbstractElement"


    // $ANTLR start "entryRuleDeclaration_de_fonction"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:116:1: entryRuleDeclaration_de_fonction : ruleDeclaration_de_fonction EOF ;
    public final void entryRuleDeclaration_de_fonction() throws RecognitionException {
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:117:1: ( ruleDeclaration_de_fonction EOF )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:118:1: ruleDeclaration_de_fonction EOF
            {
             before(grammarAccess.getDeclaration_de_fonctionRule()); 
            pushFollow(FOLLOW_ruleDeclaration_de_fonction_in_entryRuleDeclaration_de_fonction182);
            ruleDeclaration_de_fonction();

            state._fsp--;

             after(grammarAccess.getDeclaration_de_fonctionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleDeclaration_de_fonction189); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDeclaration_de_fonction"


    // $ANTLR start "ruleDeclaration_de_fonction"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:125:1: ruleDeclaration_de_fonction : ( ( rule__Declaration_de_fonction__Group__0 ) ) ;
    public final void ruleDeclaration_de_fonction() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:129:2: ( ( ( rule__Declaration_de_fonction__Group__0 ) ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:130:1: ( ( rule__Declaration_de_fonction__Group__0 ) )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:130:1: ( ( rule__Declaration_de_fonction__Group__0 ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:131:1: ( rule__Declaration_de_fonction__Group__0 )
            {
             before(grammarAccess.getDeclaration_de_fonctionAccess().getGroup()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:132:1: ( rule__Declaration_de_fonction__Group__0 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:132:2: rule__Declaration_de_fonction__Group__0
            {
            pushFollow(FOLLOW_rule__Declaration_de_fonction__Group__0_in_ruleDeclaration_de_fonction215);
            rule__Declaration_de_fonction__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDeclaration_de_fonctionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDeclaration_de_fonction"


    // $ANTLR start "entryRulefunctionContent"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:144:1: entryRulefunctionContent : rulefunctionContent EOF ;
    public final void entryRulefunctionContent() throws RecognitionException {
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:145:1: ( rulefunctionContent EOF )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:146:1: rulefunctionContent EOF
            {
             before(grammarAccess.getFunctionContentRule()); 
            pushFollow(FOLLOW_rulefunctionContent_in_entryRulefunctionContent242);
            rulefunctionContent();

            state._fsp--;

             after(grammarAccess.getFunctionContentRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulefunctionContent249); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulefunctionContent"


    // $ANTLR start "rulefunctionContent"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:153:1: rulefunctionContent : ( ( rule__FunctionContent__Alternatives ) ) ;
    public final void rulefunctionContent() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:157:2: ( ( ( rule__FunctionContent__Alternatives ) ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:158:1: ( ( rule__FunctionContent__Alternatives ) )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:158:1: ( ( rule__FunctionContent__Alternatives ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:159:1: ( rule__FunctionContent__Alternatives )
            {
             before(grammarAccess.getFunctionContentAccess().getAlternatives()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:160:1: ( rule__FunctionContent__Alternatives )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:160:2: rule__FunctionContent__Alternatives
            {
            pushFollow(FOLLOW_rule__FunctionContent__Alternatives_in_rulefunctionContent275);
            rule__FunctionContent__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getFunctionContentAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulefunctionContent"


    // $ANTLR start "entryRuleType"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:172:1: entryRuleType : ruleType EOF ;
    public final void entryRuleType() throws RecognitionException {
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:173:1: ( ruleType EOF )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:174:1: ruleType EOF
            {
             before(grammarAccess.getTypeRule()); 
            pushFollow(FOLLOW_ruleType_in_entryRuleType302);
            ruleType();

            state._fsp--;

             after(grammarAccess.getTypeRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleType309); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:181:1: ruleType : ( ( rule__Type__Group__0 ) ) ;
    public final void ruleType() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:185:2: ( ( ( rule__Type__Group__0 ) ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:186:1: ( ( rule__Type__Group__0 ) )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:186:1: ( ( rule__Type__Group__0 ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:187:1: ( rule__Type__Group__0 )
            {
             before(grammarAccess.getTypeAccess().getGroup()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:188:1: ( rule__Type__Group__0 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:188:2: rule__Type__Group__0
            {
            pushFollow(FOLLOW_rule__Type__Group__0_in_ruleType335);
            rule__Type__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleInstructions"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:200:1: entryRuleInstructions : ruleInstructions EOF ;
    public final void entryRuleInstructions() throws RecognitionException {
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:201:1: ( ruleInstructions EOF )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:202:1: ruleInstructions EOF
            {
             before(grammarAccess.getInstructionsRule()); 
            pushFollow(FOLLOW_ruleInstructions_in_entryRuleInstructions362);
            ruleInstructions();

            state._fsp--;

             after(grammarAccess.getInstructionsRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleInstructions369); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInstructions"


    // $ANTLR start "ruleInstructions"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:209:1: ruleInstructions : ( ( rule__Instructions__Alternatives ) ) ;
    public final void ruleInstructions() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:213:2: ( ( ( rule__Instructions__Alternatives ) ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:214:1: ( ( rule__Instructions__Alternatives ) )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:214:1: ( ( rule__Instructions__Alternatives ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:215:1: ( rule__Instructions__Alternatives )
            {
             before(grammarAccess.getInstructionsAccess().getAlternatives()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:216:1: ( rule__Instructions__Alternatives )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:216:2: rule__Instructions__Alternatives
            {
            pushFollow(FOLLOW_rule__Instructions__Alternatives_in_ruleInstructions395);
            rule__Instructions__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getInstructionsAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInstructions"


    // $ANTLR start "entryRuleinstanciation"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:228:1: entryRuleinstanciation : ruleinstanciation EOF ;
    public final void entryRuleinstanciation() throws RecognitionException {
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:229:1: ( ruleinstanciation EOF )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:230:1: ruleinstanciation EOF
            {
             before(grammarAccess.getInstanciationRule()); 
            pushFollow(FOLLOW_ruleinstanciation_in_entryRuleinstanciation422);
            ruleinstanciation();

            state._fsp--;

             after(grammarAccess.getInstanciationRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleinstanciation429); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleinstanciation"


    // $ANTLR start "ruleinstanciation"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:237:1: ruleinstanciation : ( ( rule__Instanciation__Group__0 ) ) ;
    public final void ruleinstanciation() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:241:2: ( ( ( rule__Instanciation__Group__0 ) ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:242:1: ( ( rule__Instanciation__Group__0 ) )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:242:1: ( ( rule__Instanciation__Group__0 ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:243:1: ( rule__Instanciation__Group__0 )
            {
             before(grammarAccess.getInstanciationAccess().getGroup()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:244:1: ( rule__Instanciation__Group__0 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:244:2: rule__Instanciation__Group__0
            {
            pushFollow(FOLLOW_rule__Instanciation__Group__0_in_ruleinstanciation455);
            rule__Instanciation__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getInstanciationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleinstanciation"


    // $ANTLR start "entryRuleaddition"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:256:1: entryRuleaddition : ruleaddition EOF ;
    public final void entryRuleaddition() throws RecognitionException {
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:257:1: ( ruleaddition EOF )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:258:1: ruleaddition EOF
            {
             before(grammarAccess.getAdditionRule()); 
            pushFollow(FOLLOW_ruleaddition_in_entryRuleaddition482);
            ruleaddition();

            state._fsp--;

             after(grammarAccess.getAdditionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleaddition489); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleaddition"


    // $ANTLR start "ruleaddition"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:265:1: ruleaddition : ( ( rule__Addition__Group__0 ) ) ;
    public final void ruleaddition() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:269:2: ( ( ( rule__Addition__Group__0 ) ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:270:1: ( ( rule__Addition__Group__0 ) )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:270:1: ( ( rule__Addition__Group__0 ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:271:1: ( rule__Addition__Group__0 )
            {
             before(grammarAccess.getAdditionAccess().getGroup()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:272:1: ( rule__Addition__Group__0 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:272:2: rule__Addition__Group__0
            {
            pushFollow(FOLLOW_rule__Addition__Group__0_in_ruleaddition515);
            rule__Addition__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAdditionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleaddition"


    // $ANTLR start "entryRulealert"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:284:1: entryRulealert : rulealert EOF ;
    public final void entryRulealert() throws RecognitionException {
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:285:1: ( rulealert EOF )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:286:1: rulealert EOF
            {
             before(grammarAccess.getAlertRule()); 
            pushFollow(FOLLOW_rulealert_in_entryRulealert542);
            rulealert();

            state._fsp--;

             after(grammarAccess.getAlertRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulealert549); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulealert"


    // $ANTLR start "rulealert"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:293:1: rulealert : ( ( rule__Alert__Group__0 ) ) ;
    public final void rulealert() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:297:2: ( ( ( rule__Alert__Group__0 ) ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:298:1: ( ( rule__Alert__Group__0 ) )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:298:1: ( ( rule__Alert__Group__0 ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:299:1: ( rule__Alert__Group__0 )
            {
             before(grammarAccess.getAlertAccess().getGroup()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:300:1: ( rule__Alert__Group__0 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:300:2: rule__Alert__Group__0
            {
            pushFollow(FOLLOW_rule__Alert__Group__0_in_rulealert575);
            rule__Alert__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAlertAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulealert"


    // $ANTLR start "entryRuleprompt"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:312:1: entryRuleprompt : ruleprompt EOF ;
    public final void entryRuleprompt() throws RecognitionException {
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:313:1: ( ruleprompt EOF )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:314:1: ruleprompt EOF
            {
             before(grammarAccess.getPromptRule()); 
            pushFollow(FOLLOW_ruleprompt_in_entryRuleprompt602);
            ruleprompt();

            state._fsp--;

             after(grammarAccess.getPromptRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleprompt609); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleprompt"


    // $ANTLR start "ruleprompt"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:321:1: ruleprompt : ( ( rule__Prompt__Group__0 ) ) ;
    public final void ruleprompt() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:325:2: ( ( ( rule__Prompt__Group__0 ) ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:326:1: ( ( rule__Prompt__Group__0 ) )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:326:1: ( ( rule__Prompt__Group__0 ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:327:1: ( rule__Prompt__Group__0 )
            {
             before(grammarAccess.getPromptAccess().getGroup()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:328:1: ( rule__Prompt__Group__0 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:328:2: rule__Prompt__Group__0
            {
            pushFollow(FOLLOW_rule__Prompt__Group__0_in_ruleprompt635);
            rule__Prompt__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPromptAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleprompt"


    // $ANTLR start "rule__AbstractElement__Alternatives"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:340:1: rule__AbstractElement__Alternatives : ( ( ruleDeclaration_de_fonction ) | ( ruleType ) | ( ruleInstructions ) );
    public final void rule__AbstractElement__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:344:1: ( ( ruleDeclaration_de_fonction ) | ( ruleType ) | ( ruleInstructions ) )
            int alt2=3;
            switch ( input.LA(1) ) {
            case 11:
                {
                alt2=1;
                }
                break;
            case 16:
                {
                int LA2_2 = input.LA(2);

                if ( (LA2_2==RULE_ID) ) {
                    int LA2_4 = input.LA(3);

                    if ( (LA2_4==18) ) {
                        alt2=3;
                    }
                    else if ( (LA2_4==17) ) {
                        alt2=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 2, 4, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 2, input);

                    throw nvae;
                }
                }
                break;
            case RULE_ID:
            case 20:
            case 22:
                {
                alt2=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:345:1: ( ruleDeclaration_de_fonction )
                    {
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:345:1: ( ruleDeclaration_de_fonction )
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:346:1: ruleDeclaration_de_fonction
                    {
                     before(grammarAccess.getAbstractElementAccess().getDeclaration_de_fonctionParserRuleCall_0()); 
                    pushFollow(FOLLOW_ruleDeclaration_de_fonction_in_rule__AbstractElement__Alternatives671);
                    ruleDeclaration_de_fonction();

                    state._fsp--;

                     after(grammarAccess.getAbstractElementAccess().getDeclaration_de_fonctionParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:351:6: ( ruleType )
                    {
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:351:6: ( ruleType )
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:352:1: ruleType
                    {
                     before(grammarAccess.getAbstractElementAccess().getTypeParserRuleCall_1()); 
                    pushFollow(FOLLOW_ruleType_in_rule__AbstractElement__Alternatives688);
                    ruleType();

                    state._fsp--;

                     after(grammarAccess.getAbstractElementAccess().getTypeParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:357:6: ( ruleInstructions )
                    {
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:357:6: ( ruleInstructions )
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:358:1: ruleInstructions
                    {
                     before(grammarAccess.getAbstractElementAccess().getInstructionsParserRuleCall_2()); 
                    pushFollow(FOLLOW_ruleInstructions_in_rule__AbstractElement__Alternatives705);
                    ruleInstructions();

                    state._fsp--;

                     after(grammarAccess.getAbstractElementAccess().getInstructionsParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AbstractElement__Alternatives"


    // $ANTLR start "rule__FunctionContent__Alternatives"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:368:1: rule__FunctionContent__Alternatives : ( ( ruleInstructions ) | ( ruleType ) );
    public final void rule__FunctionContent__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:372:1: ( ( ruleInstructions ) | ( ruleType ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==16) ) {
                int LA3_1 = input.LA(2);

                if ( (LA3_1==RULE_ID) ) {
                    int LA3_3 = input.LA(3);

                    if ( (LA3_3==18) ) {
                        alt3=1;
                    }
                    else if ( (LA3_3==17) ) {
                        alt3=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 3, 3, input);

                        throw nvae;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 3, 1, input);

                    throw nvae;
                }
            }
            else if ( (LA3_0==RULE_ID||LA3_0==20||LA3_0==22) ) {
                alt3=1;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:373:1: ( ruleInstructions )
                    {
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:373:1: ( ruleInstructions )
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:374:1: ruleInstructions
                    {
                     before(grammarAccess.getFunctionContentAccess().getInstructionsParserRuleCall_0()); 
                    pushFollow(FOLLOW_ruleInstructions_in_rule__FunctionContent__Alternatives737);
                    ruleInstructions();

                    state._fsp--;

                     after(grammarAccess.getFunctionContentAccess().getInstructionsParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:379:6: ( ruleType )
                    {
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:379:6: ( ruleType )
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:380:1: ruleType
                    {
                     before(grammarAccess.getFunctionContentAccess().getTypeParserRuleCall_1()); 
                    pushFollow(FOLLOW_ruleType_in_rule__FunctionContent__Alternatives754);
                    ruleType();

                    state._fsp--;

                     after(grammarAccess.getFunctionContentAccess().getTypeParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionContent__Alternatives"


    // $ANTLR start "rule__Instructions__Alternatives"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:390:1: rule__Instructions__Alternatives : ( ( ruleinstanciation ) | ( ruleaddition ) | ( rulealert ) | ( ruleprompt ) | ( ( rule__Instructions__Group_4__0 ) ) );
    public final void rule__Instructions__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:394:1: ( ( ruleinstanciation ) | ( ruleaddition ) | ( rulealert ) | ( ruleprompt ) | ( ( rule__Instructions__Group_4__0 ) ) )
            int alt4=5;
            alt4 = dfa4.predict(input);
            switch (alt4) {
                case 1 :
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:395:1: ( ruleinstanciation )
                    {
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:395:1: ( ruleinstanciation )
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:396:1: ruleinstanciation
                    {
                     before(grammarAccess.getInstructionsAccess().getInstanciationParserRuleCall_0()); 
                    pushFollow(FOLLOW_ruleinstanciation_in_rule__Instructions__Alternatives786);
                    ruleinstanciation();

                    state._fsp--;

                     after(grammarAccess.getInstructionsAccess().getInstanciationParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:401:6: ( ruleaddition )
                    {
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:401:6: ( ruleaddition )
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:402:1: ruleaddition
                    {
                     before(grammarAccess.getInstructionsAccess().getAdditionParserRuleCall_1()); 
                    pushFollow(FOLLOW_ruleaddition_in_rule__Instructions__Alternatives803);
                    ruleaddition();

                    state._fsp--;

                     after(grammarAccess.getInstructionsAccess().getAdditionParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:407:6: ( rulealert )
                    {
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:407:6: ( rulealert )
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:408:1: rulealert
                    {
                     before(grammarAccess.getInstructionsAccess().getAlertParserRuleCall_2()); 
                    pushFollow(FOLLOW_rulealert_in_rule__Instructions__Alternatives820);
                    rulealert();

                    state._fsp--;

                     after(grammarAccess.getInstructionsAccess().getAlertParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:413:6: ( ruleprompt )
                    {
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:413:6: ( ruleprompt )
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:414:1: ruleprompt
                    {
                     before(grammarAccess.getInstructionsAccess().getPromptParserRuleCall_3()); 
                    pushFollow(FOLLOW_ruleprompt_in_rule__Instructions__Alternatives837);
                    ruleprompt();

                    state._fsp--;

                     after(grammarAccess.getInstructionsAccess().getPromptParserRuleCall_3()); 

                    }


                    }
                    break;
                case 5 :
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:419:6: ( ( rule__Instructions__Group_4__0 ) )
                    {
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:419:6: ( ( rule__Instructions__Group_4__0 ) )
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:420:1: ( rule__Instructions__Group_4__0 )
                    {
                     before(grammarAccess.getInstructionsAccess().getGroup_4()); 
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:421:1: ( rule__Instructions__Group_4__0 )
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:421:2: rule__Instructions__Group_4__0
                    {
                    pushFollow(FOLLOW_rule__Instructions__Group_4__0_in_rule__Instructions__Alternatives854);
                    rule__Instructions__Group_4__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getInstructionsAccess().getGroup_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instructions__Alternatives"


    // $ANTLR start "rule__Declaration_de_fonction__Group__0"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:432:1: rule__Declaration_de_fonction__Group__0 : rule__Declaration_de_fonction__Group__0__Impl rule__Declaration_de_fonction__Group__1 ;
    public final void rule__Declaration_de_fonction__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:436:1: ( rule__Declaration_de_fonction__Group__0__Impl rule__Declaration_de_fonction__Group__1 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:437:2: rule__Declaration_de_fonction__Group__0__Impl rule__Declaration_de_fonction__Group__1
            {
            pushFollow(FOLLOW_rule__Declaration_de_fonction__Group__0__Impl_in_rule__Declaration_de_fonction__Group__0885);
            rule__Declaration_de_fonction__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Declaration_de_fonction__Group__1_in_rule__Declaration_de_fonction__Group__0888);
            rule__Declaration_de_fonction__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_de_fonction__Group__0"


    // $ANTLR start "rule__Declaration_de_fonction__Group__0__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:444:1: rule__Declaration_de_fonction__Group__0__Impl : ( 'function' ) ;
    public final void rule__Declaration_de_fonction__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:448:1: ( ( 'function' ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:449:1: ( 'function' )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:449:1: ( 'function' )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:450:1: 'function'
            {
             before(grammarAccess.getDeclaration_de_fonctionAccess().getFunctionKeyword_0()); 
            match(input,11,FOLLOW_11_in_rule__Declaration_de_fonction__Group__0__Impl916); 
             after(grammarAccess.getDeclaration_de_fonctionAccess().getFunctionKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_de_fonction__Group__0__Impl"


    // $ANTLR start "rule__Declaration_de_fonction__Group__1"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:463:1: rule__Declaration_de_fonction__Group__1 : rule__Declaration_de_fonction__Group__1__Impl rule__Declaration_de_fonction__Group__2 ;
    public final void rule__Declaration_de_fonction__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:467:1: ( rule__Declaration_de_fonction__Group__1__Impl rule__Declaration_de_fonction__Group__2 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:468:2: rule__Declaration_de_fonction__Group__1__Impl rule__Declaration_de_fonction__Group__2
            {
            pushFollow(FOLLOW_rule__Declaration_de_fonction__Group__1__Impl_in_rule__Declaration_de_fonction__Group__1947);
            rule__Declaration_de_fonction__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Declaration_de_fonction__Group__2_in_rule__Declaration_de_fonction__Group__1950);
            rule__Declaration_de_fonction__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_de_fonction__Group__1"


    // $ANTLR start "rule__Declaration_de_fonction__Group__1__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:475:1: rule__Declaration_de_fonction__Group__1__Impl : ( ( rule__Declaration_de_fonction__FunctionNameAssignment_1 ) ) ;
    public final void rule__Declaration_de_fonction__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:479:1: ( ( ( rule__Declaration_de_fonction__FunctionNameAssignment_1 ) ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:480:1: ( ( rule__Declaration_de_fonction__FunctionNameAssignment_1 ) )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:480:1: ( ( rule__Declaration_de_fonction__FunctionNameAssignment_1 ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:481:1: ( rule__Declaration_de_fonction__FunctionNameAssignment_1 )
            {
             before(grammarAccess.getDeclaration_de_fonctionAccess().getFunctionNameAssignment_1()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:482:1: ( rule__Declaration_de_fonction__FunctionNameAssignment_1 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:482:2: rule__Declaration_de_fonction__FunctionNameAssignment_1
            {
            pushFollow(FOLLOW_rule__Declaration_de_fonction__FunctionNameAssignment_1_in_rule__Declaration_de_fonction__Group__1__Impl977);
            rule__Declaration_de_fonction__FunctionNameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDeclaration_de_fonctionAccess().getFunctionNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_de_fonction__Group__1__Impl"


    // $ANTLR start "rule__Declaration_de_fonction__Group__2"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:492:1: rule__Declaration_de_fonction__Group__2 : rule__Declaration_de_fonction__Group__2__Impl rule__Declaration_de_fonction__Group__3 ;
    public final void rule__Declaration_de_fonction__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:496:1: ( rule__Declaration_de_fonction__Group__2__Impl rule__Declaration_de_fonction__Group__3 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:497:2: rule__Declaration_de_fonction__Group__2__Impl rule__Declaration_de_fonction__Group__3
            {
            pushFollow(FOLLOW_rule__Declaration_de_fonction__Group__2__Impl_in_rule__Declaration_de_fonction__Group__21007);
            rule__Declaration_de_fonction__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Declaration_de_fonction__Group__3_in_rule__Declaration_de_fonction__Group__21010);
            rule__Declaration_de_fonction__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_de_fonction__Group__2"


    // $ANTLR start "rule__Declaration_de_fonction__Group__2__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:504:1: rule__Declaration_de_fonction__Group__2__Impl : ( '(' ) ;
    public final void rule__Declaration_de_fonction__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:508:1: ( ( '(' ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:509:1: ( '(' )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:509:1: ( '(' )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:510:1: '('
            {
             before(grammarAccess.getDeclaration_de_fonctionAccess().getLeftParenthesisKeyword_2()); 
            match(input,12,FOLLOW_12_in_rule__Declaration_de_fonction__Group__2__Impl1038); 
             after(grammarAccess.getDeclaration_de_fonctionAccess().getLeftParenthesisKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_de_fonction__Group__2__Impl"


    // $ANTLR start "rule__Declaration_de_fonction__Group__3"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:523:1: rule__Declaration_de_fonction__Group__3 : rule__Declaration_de_fonction__Group__3__Impl rule__Declaration_de_fonction__Group__4 ;
    public final void rule__Declaration_de_fonction__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:527:1: ( rule__Declaration_de_fonction__Group__3__Impl rule__Declaration_de_fonction__Group__4 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:528:2: rule__Declaration_de_fonction__Group__3__Impl rule__Declaration_de_fonction__Group__4
            {
            pushFollow(FOLLOW_rule__Declaration_de_fonction__Group__3__Impl_in_rule__Declaration_de_fonction__Group__31069);
            rule__Declaration_de_fonction__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Declaration_de_fonction__Group__4_in_rule__Declaration_de_fonction__Group__31072);
            rule__Declaration_de_fonction__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_de_fonction__Group__3"


    // $ANTLR start "rule__Declaration_de_fonction__Group__3__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:535:1: rule__Declaration_de_fonction__Group__3__Impl : ( ( rule__Declaration_de_fonction__ArgumentsAssignment_3 )? ) ;
    public final void rule__Declaration_de_fonction__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:539:1: ( ( ( rule__Declaration_de_fonction__ArgumentsAssignment_3 )? ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:540:1: ( ( rule__Declaration_de_fonction__ArgumentsAssignment_3 )? )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:540:1: ( ( rule__Declaration_de_fonction__ArgumentsAssignment_3 )? )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:541:1: ( rule__Declaration_de_fonction__ArgumentsAssignment_3 )?
            {
             before(grammarAccess.getDeclaration_de_fonctionAccess().getArgumentsAssignment_3()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:542:1: ( rule__Declaration_de_fonction__ArgumentsAssignment_3 )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==RULE_ID) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:542:2: rule__Declaration_de_fonction__ArgumentsAssignment_3
                    {
                    pushFollow(FOLLOW_rule__Declaration_de_fonction__ArgumentsAssignment_3_in_rule__Declaration_de_fonction__Group__3__Impl1099);
                    rule__Declaration_de_fonction__ArgumentsAssignment_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDeclaration_de_fonctionAccess().getArgumentsAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_de_fonction__Group__3__Impl"


    // $ANTLR start "rule__Declaration_de_fonction__Group__4"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:552:1: rule__Declaration_de_fonction__Group__4 : rule__Declaration_de_fonction__Group__4__Impl rule__Declaration_de_fonction__Group__5 ;
    public final void rule__Declaration_de_fonction__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:556:1: ( rule__Declaration_de_fonction__Group__4__Impl rule__Declaration_de_fonction__Group__5 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:557:2: rule__Declaration_de_fonction__Group__4__Impl rule__Declaration_de_fonction__Group__5
            {
            pushFollow(FOLLOW_rule__Declaration_de_fonction__Group__4__Impl_in_rule__Declaration_de_fonction__Group__41130);
            rule__Declaration_de_fonction__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Declaration_de_fonction__Group__5_in_rule__Declaration_de_fonction__Group__41133);
            rule__Declaration_de_fonction__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_de_fonction__Group__4"


    // $ANTLR start "rule__Declaration_de_fonction__Group__4__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:564:1: rule__Declaration_de_fonction__Group__4__Impl : ( ( rule__Declaration_de_fonction__Group_4__0 )* ) ;
    public final void rule__Declaration_de_fonction__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:568:1: ( ( ( rule__Declaration_de_fonction__Group_4__0 )* ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:569:1: ( ( rule__Declaration_de_fonction__Group_4__0 )* )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:569:1: ( ( rule__Declaration_de_fonction__Group_4__0 )* )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:570:1: ( rule__Declaration_de_fonction__Group_4__0 )*
            {
             before(grammarAccess.getDeclaration_de_fonctionAccess().getGroup_4()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:571:1: ( rule__Declaration_de_fonction__Group_4__0 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==15) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:571:2: rule__Declaration_de_fonction__Group_4__0
            	    {
            	    pushFollow(FOLLOW_rule__Declaration_de_fonction__Group_4__0_in_rule__Declaration_de_fonction__Group__4__Impl1160);
            	    rule__Declaration_de_fonction__Group_4__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getDeclaration_de_fonctionAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_de_fonction__Group__4__Impl"


    // $ANTLR start "rule__Declaration_de_fonction__Group__5"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:581:1: rule__Declaration_de_fonction__Group__5 : rule__Declaration_de_fonction__Group__5__Impl rule__Declaration_de_fonction__Group__6 ;
    public final void rule__Declaration_de_fonction__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:585:1: ( rule__Declaration_de_fonction__Group__5__Impl rule__Declaration_de_fonction__Group__6 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:586:2: rule__Declaration_de_fonction__Group__5__Impl rule__Declaration_de_fonction__Group__6
            {
            pushFollow(FOLLOW_rule__Declaration_de_fonction__Group__5__Impl_in_rule__Declaration_de_fonction__Group__51191);
            rule__Declaration_de_fonction__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Declaration_de_fonction__Group__6_in_rule__Declaration_de_fonction__Group__51194);
            rule__Declaration_de_fonction__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_de_fonction__Group__5"


    // $ANTLR start "rule__Declaration_de_fonction__Group__5__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:593:1: rule__Declaration_de_fonction__Group__5__Impl : ( '){' ) ;
    public final void rule__Declaration_de_fonction__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:597:1: ( ( '){' ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:598:1: ( '){' )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:598:1: ( '){' )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:599:1: '){'
            {
             before(grammarAccess.getDeclaration_de_fonctionAccess().getRightParenthesisLeftCurlyBracketKeyword_5()); 
            match(input,13,FOLLOW_13_in_rule__Declaration_de_fonction__Group__5__Impl1222); 
             after(grammarAccess.getDeclaration_de_fonctionAccess().getRightParenthesisLeftCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_de_fonction__Group__5__Impl"


    // $ANTLR start "rule__Declaration_de_fonction__Group__6"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:612:1: rule__Declaration_de_fonction__Group__6 : rule__Declaration_de_fonction__Group__6__Impl rule__Declaration_de_fonction__Group__7 ;
    public final void rule__Declaration_de_fonction__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:616:1: ( rule__Declaration_de_fonction__Group__6__Impl rule__Declaration_de_fonction__Group__7 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:617:2: rule__Declaration_de_fonction__Group__6__Impl rule__Declaration_de_fonction__Group__7
            {
            pushFollow(FOLLOW_rule__Declaration_de_fonction__Group__6__Impl_in_rule__Declaration_de_fonction__Group__61253);
            rule__Declaration_de_fonction__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Declaration_de_fonction__Group__7_in_rule__Declaration_de_fonction__Group__61256);
            rule__Declaration_de_fonction__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_de_fonction__Group__6"


    // $ANTLR start "rule__Declaration_de_fonction__Group__6__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:624:1: rule__Declaration_de_fonction__Group__6__Impl : ( ( ( rule__Declaration_de_fonction__ElementAssignment_6 ) ) ( ( rule__Declaration_de_fonction__ElementAssignment_6 )* ) ) ;
    public final void rule__Declaration_de_fonction__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:628:1: ( ( ( ( rule__Declaration_de_fonction__ElementAssignment_6 ) ) ( ( rule__Declaration_de_fonction__ElementAssignment_6 )* ) ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:629:1: ( ( ( rule__Declaration_de_fonction__ElementAssignment_6 ) ) ( ( rule__Declaration_de_fonction__ElementAssignment_6 )* ) )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:629:1: ( ( ( rule__Declaration_de_fonction__ElementAssignment_6 ) ) ( ( rule__Declaration_de_fonction__ElementAssignment_6 )* ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:630:1: ( ( rule__Declaration_de_fonction__ElementAssignment_6 ) ) ( ( rule__Declaration_de_fonction__ElementAssignment_6 )* )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:630:1: ( ( rule__Declaration_de_fonction__ElementAssignment_6 ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:631:1: ( rule__Declaration_de_fonction__ElementAssignment_6 )
            {
             before(grammarAccess.getDeclaration_de_fonctionAccess().getElementAssignment_6()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:632:1: ( rule__Declaration_de_fonction__ElementAssignment_6 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:632:2: rule__Declaration_de_fonction__ElementAssignment_6
            {
            pushFollow(FOLLOW_rule__Declaration_de_fonction__ElementAssignment_6_in_rule__Declaration_de_fonction__Group__6__Impl1285);
            rule__Declaration_de_fonction__ElementAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getDeclaration_de_fonctionAccess().getElementAssignment_6()); 

            }

            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:635:1: ( ( rule__Declaration_de_fonction__ElementAssignment_6 )* )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:636:1: ( rule__Declaration_de_fonction__ElementAssignment_6 )*
            {
             before(grammarAccess.getDeclaration_de_fonctionAccess().getElementAssignment_6()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:637:1: ( rule__Declaration_de_fonction__ElementAssignment_6 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==RULE_ID||LA7_0==16||LA7_0==20||LA7_0==22) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:637:2: rule__Declaration_de_fonction__ElementAssignment_6
            	    {
            	    pushFollow(FOLLOW_rule__Declaration_de_fonction__ElementAssignment_6_in_rule__Declaration_de_fonction__Group__6__Impl1297);
            	    rule__Declaration_de_fonction__ElementAssignment_6();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getDeclaration_de_fonctionAccess().getElementAssignment_6()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_de_fonction__Group__6__Impl"


    // $ANTLR start "rule__Declaration_de_fonction__Group__7"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:648:1: rule__Declaration_de_fonction__Group__7 : rule__Declaration_de_fonction__Group__7__Impl ;
    public final void rule__Declaration_de_fonction__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:652:1: ( rule__Declaration_de_fonction__Group__7__Impl )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:653:2: rule__Declaration_de_fonction__Group__7__Impl
            {
            pushFollow(FOLLOW_rule__Declaration_de_fonction__Group__7__Impl_in_rule__Declaration_de_fonction__Group__71330);
            rule__Declaration_de_fonction__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_de_fonction__Group__7"


    // $ANTLR start "rule__Declaration_de_fonction__Group__7__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:659:1: rule__Declaration_de_fonction__Group__7__Impl : ( '}' ) ;
    public final void rule__Declaration_de_fonction__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:663:1: ( ( '}' ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:664:1: ( '}' )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:664:1: ( '}' )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:665:1: '}'
            {
             before(grammarAccess.getDeclaration_de_fonctionAccess().getRightCurlyBracketKeyword_7()); 
            match(input,14,FOLLOW_14_in_rule__Declaration_de_fonction__Group__7__Impl1358); 
             after(grammarAccess.getDeclaration_de_fonctionAccess().getRightCurlyBracketKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_de_fonction__Group__7__Impl"


    // $ANTLR start "rule__Declaration_de_fonction__Group_4__0"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:694:1: rule__Declaration_de_fonction__Group_4__0 : rule__Declaration_de_fonction__Group_4__0__Impl rule__Declaration_de_fonction__Group_4__1 ;
    public final void rule__Declaration_de_fonction__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:698:1: ( rule__Declaration_de_fonction__Group_4__0__Impl rule__Declaration_de_fonction__Group_4__1 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:699:2: rule__Declaration_de_fonction__Group_4__0__Impl rule__Declaration_de_fonction__Group_4__1
            {
            pushFollow(FOLLOW_rule__Declaration_de_fonction__Group_4__0__Impl_in_rule__Declaration_de_fonction__Group_4__01405);
            rule__Declaration_de_fonction__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Declaration_de_fonction__Group_4__1_in_rule__Declaration_de_fonction__Group_4__01408);
            rule__Declaration_de_fonction__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_de_fonction__Group_4__0"


    // $ANTLR start "rule__Declaration_de_fonction__Group_4__0__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:706:1: rule__Declaration_de_fonction__Group_4__0__Impl : ( ',' ) ;
    public final void rule__Declaration_de_fonction__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:710:1: ( ( ',' ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:711:1: ( ',' )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:711:1: ( ',' )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:712:1: ','
            {
             before(grammarAccess.getDeclaration_de_fonctionAccess().getCommaKeyword_4_0()); 
            match(input,15,FOLLOW_15_in_rule__Declaration_de_fonction__Group_4__0__Impl1436); 
             after(grammarAccess.getDeclaration_de_fonctionAccess().getCommaKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_de_fonction__Group_4__0__Impl"


    // $ANTLR start "rule__Declaration_de_fonction__Group_4__1"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:725:1: rule__Declaration_de_fonction__Group_4__1 : rule__Declaration_de_fonction__Group_4__1__Impl ;
    public final void rule__Declaration_de_fonction__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:729:1: ( rule__Declaration_de_fonction__Group_4__1__Impl )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:730:2: rule__Declaration_de_fonction__Group_4__1__Impl
            {
            pushFollow(FOLLOW_rule__Declaration_de_fonction__Group_4__1__Impl_in_rule__Declaration_de_fonction__Group_4__11467);
            rule__Declaration_de_fonction__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_de_fonction__Group_4__1"


    // $ANTLR start "rule__Declaration_de_fonction__Group_4__1__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:736:1: rule__Declaration_de_fonction__Group_4__1__Impl : ( ( rule__Declaration_de_fonction__ArgumentAssignment_4_1 ) ) ;
    public final void rule__Declaration_de_fonction__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:740:1: ( ( ( rule__Declaration_de_fonction__ArgumentAssignment_4_1 ) ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:741:1: ( ( rule__Declaration_de_fonction__ArgumentAssignment_4_1 ) )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:741:1: ( ( rule__Declaration_de_fonction__ArgumentAssignment_4_1 ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:742:1: ( rule__Declaration_de_fonction__ArgumentAssignment_4_1 )
            {
             before(grammarAccess.getDeclaration_de_fonctionAccess().getArgumentAssignment_4_1()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:743:1: ( rule__Declaration_de_fonction__ArgumentAssignment_4_1 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:743:2: rule__Declaration_de_fonction__ArgumentAssignment_4_1
            {
            pushFollow(FOLLOW_rule__Declaration_de_fonction__ArgumentAssignment_4_1_in_rule__Declaration_de_fonction__Group_4__1__Impl1494);
            rule__Declaration_de_fonction__ArgumentAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getDeclaration_de_fonctionAccess().getArgumentAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_de_fonction__Group_4__1__Impl"


    // $ANTLR start "rule__Type__Group__0"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:757:1: rule__Type__Group__0 : rule__Type__Group__0__Impl rule__Type__Group__1 ;
    public final void rule__Type__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:761:1: ( rule__Type__Group__0__Impl rule__Type__Group__1 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:762:2: rule__Type__Group__0__Impl rule__Type__Group__1
            {
            pushFollow(FOLLOW_rule__Type__Group__0__Impl_in_rule__Type__Group__01528);
            rule__Type__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Type__Group__1_in_rule__Type__Group__01531);
            rule__Type__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__0"


    // $ANTLR start "rule__Type__Group__0__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:769:1: rule__Type__Group__0__Impl : ( 'var' ) ;
    public final void rule__Type__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:773:1: ( ( 'var' ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:774:1: ( 'var' )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:774:1: ( 'var' )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:775:1: 'var'
            {
             before(grammarAccess.getTypeAccess().getVarKeyword_0()); 
            match(input,16,FOLLOW_16_in_rule__Type__Group__0__Impl1559); 
             after(grammarAccess.getTypeAccess().getVarKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__0__Impl"


    // $ANTLR start "rule__Type__Group__1"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:788:1: rule__Type__Group__1 : rule__Type__Group__1__Impl rule__Type__Group__2 ;
    public final void rule__Type__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:792:1: ( rule__Type__Group__1__Impl rule__Type__Group__2 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:793:2: rule__Type__Group__1__Impl rule__Type__Group__2
            {
            pushFollow(FOLLOW_rule__Type__Group__1__Impl_in_rule__Type__Group__11590);
            rule__Type__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Type__Group__2_in_rule__Type__Group__11593);
            rule__Type__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__1"


    // $ANTLR start "rule__Type__Group__1__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:800:1: rule__Type__Group__1__Impl : ( ( rule__Type__NameAssignment_1 ) ) ;
    public final void rule__Type__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:804:1: ( ( ( rule__Type__NameAssignment_1 ) ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:805:1: ( ( rule__Type__NameAssignment_1 ) )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:805:1: ( ( rule__Type__NameAssignment_1 ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:806:1: ( rule__Type__NameAssignment_1 )
            {
             before(grammarAccess.getTypeAccess().getNameAssignment_1()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:807:1: ( rule__Type__NameAssignment_1 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:807:2: rule__Type__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Type__NameAssignment_1_in_rule__Type__Group__1__Impl1620);
            rule__Type__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getTypeAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__1__Impl"


    // $ANTLR start "rule__Type__Group__2"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:817:1: rule__Type__Group__2 : rule__Type__Group__2__Impl ;
    public final void rule__Type__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:821:1: ( rule__Type__Group__2__Impl )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:822:2: rule__Type__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Type__Group__2__Impl_in_rule__Type__Group__21650);
            rule__Type__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__2"


    // $ANTLR start "rule__Type__Group__2__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:828:1: rule__Type__Group__2__Impl : ( ';' ) ;
    public final void rule__Type__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:832:1: ( ( ';' ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:833:1: ( ';' )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:833:1: ( ';' )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:834:1: ';'
            {
             before(grammarAccess.getTypeAccess().getSemicolonKeyword_2()); 
            match(input,17,FOLLOW_17_in_rule__Type__Group__2__Impl1678); 
             after(grammarAccess.getTypeAccess().getSemicolonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__2__Impl"


    // $ANTLR start "rule__Instructions__Group_4__0"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:853:1: rule__Instructions__Group_4__0 : rule__Instructions__Group_4__0__Impl rule__Instructions__Group_4__1 ;
    public final void rule__Instructions__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:857:1: ( rule__Instructions__Group_4__0__Impl rule__Instructions__Group_4__1 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:858:2: rule__Instructions__Group_4__0__Impl rule__Instructions__Group_4__1
            {
            pushFollow(FOLLOW_rule__Instructions__Group_4__0__Impl_in_rule__Instructions__Group_4__01715);
            rule__Instructions__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Instructions__Group_4__1_in_rule__Instructions__Group_4__01718);
            rule__Instructions__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instructions__Group_4__0"


    // $ANTLR start "rule__Instructions__Group_4__0__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:865:1: rule__Instructions__Group_4__0__Impl : ( ( rule__Instructions__NameAssignment_4_0 ) ) ;
    public final void rule__Instructions__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:869:1: ( ( ( rule__Instructions__NameAssignment_4_0 ) ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:870:1: ( ( rule__Instructions__NameAssignment_4_0 ) )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:870:1: ( ( rule__Instructions__NameAssignment_4_0 ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:871:1: ( rule__Instructions__NameAssignment_4_0 )
            {
             before(grammarAccess.getInstructionsAccess().getNameAssignment_4_0()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:872:1: ( rule__Instructions__NameAssignment_4_0 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:872:2: rule__Instructions__NameAssignment_4_0
            {
            pushFollow(FOLLOW_rule__Instructions__NameAssignment_4_0_in_rule__Instructions__Group_4__0__Impl1745);
            rule__Instructions__NameAssignment_4_0();

            state._fsp--;


            }

             after(grammarAccess.getInstructionsAccess().getNameAssignment_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instructions__Group_4__0__Impl"


    // $ANTLR start "rule__Instructions__Group_4__1"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:882:1: rule__Instructions__Group_4__1 : rule__Instructions__Group_4__1__Impl ;
    public final void rule__Instructions__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:886:1: ( rule__Instructions__Group_4__1__Impl )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:887:2: rule__Instructions__Group_4__1__Impl
            {
            pushFollow(FOLLOW_rule__Instructions__Group_4__1__Impl_in_rule__Instructions__Group_4__11775);
            rule__Instructions__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instructions__Group_4__1"


    // $ANTLR start "rule__Instructions__Group_4__1__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:893:1: rule__Instructions__Group_4__1__Impl : ( ';' ) ;
    public final void rule__Instructions__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:897:1: ( ( ';' ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:898:1: ( ';' )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:898:1: ( ';' )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:899:1: ';'
            {
             before(grammarAccess.getInstructionsAccess().getSemicolonKeyword_4_1()); 
            match(input,17,FOLLOW_17_in_rule__Instructions__Group_4__1__Impl1803); 
             after(grammarAccess.getInstructionsAccess().getSemicolonKeyword_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instructions__Group_4__1__Impl"


    // $ANTLR start "rule__Instanciation__Group__0"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:916:1: rule__Instanciation__Group__0 : rule__Instanciation__Group__0__Impl rule__Instanciation__Group__1 ;
    public final void rule__Instanciation__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:920:1: ( rule__Instanciation__Group__0__Impl rule__Instanciation__Group__1 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:921:2: rule__Instanciation__Group__0__Impl rule__Instanciation__Group__1
            {
            pushFollow(FOLLOW_rule__Instanciation__Group__0__Impl_in_rule__Instanciation__Group__01838);
            rule__Instanciation__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Instanciation__Group__1_in_rule__Instanciation__Group__01841);
            rule__Instanciation__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instanciation__Group__0"


    // $ANTLR start "rule__Instanciation__Group__0__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:928:1: rule__Instanciation__Group__0__Impl : ( ( 'var' )? ) ;
    public final void rule__Instanciation__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:932:1: ( ( ( 'var' )? ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:933:1: ( ( 'var' )? )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:933:1: ( ( 'var' )? )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:934:1: ( 'var' )?
            {
             before(grammarAccess.getInstanciationAccess().getVarKeyword_0()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:935:1: ( 'var' )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==16) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:936:2: 'var'
                    {
                    match(input,16,FOLLOW_16_in_rule__Instanciation__Group__0__Impl1870); 

                    }
                    break;

            }

             after(grammarAccess.getInstanciationAccess().getVarKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instanciation__Group__0__Impl"


    // $ANTLR start "rule__Instanciation__Group__1"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:947:1: rule__Instanciation__Group__1 : rule__Instanciation__Group__1__Impl rule__Instanciation__Group__2 ;
    public final void rule__Instanciation__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:951:1: ( rule__Instanciation__Group__1__Impl rule__Instanciation__Group__2 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:952:2: rule__Instanciation__Group__1__Impl rule__Instanciation__Group__2
            {
            pushFollow(FOLLOW_rule__Instanciation__Group__1__Impl_in_rule__Instanciation__Group__11903);
            rule__Instanciation__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Instanciation__Group__2_in_rule__Instanciation__Group__11906);
            rule__Instanciation__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instanciation__Group__1"


    // $ANTLR start "rule__Instanciation__Group__1__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:959:1: rule__Instanciation__Group__1__Impl : ( ( rule__Instanciation__NameAssignment_1 ) ) ;
    public final void rule__Instanciation__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:963:1: ( ( ( rule__Instanciation__NameAssignment_1 ) ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:964:1: ( ( rule__Instanciation__NameAssignment_1 ) )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:964:1: ( ( rule__Instanciation__NameAssignment_1 ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:965:1: ( rule__Instanciation__NameAssignment_1 )
            {
             before(grammarAccess.getInstanciationAccess().getNameAssignment_1()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:966:1: ( rule__Instanciation__NameAssignment_1 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:966:2: rule__Instanciation__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Instanciation__NameAssignment_1_in_rule__Instanciation__Group__1__Impl1933);
            rule__Instanciation__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getInstanciationAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instanciation__Group__1__Impl"


    // $ANTLR start "rule__Instanciation__Group__2"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:976:1: rule__Instanciation__Group__2 : rule__Instanciation__Group__2__Impl rule__Instanciation__Group__3 ;
    public final void rule__Instanciation__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:980:1: ( rule__Instanciation__Group__2__Impl rule__Instanciation__Group__3 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:981:2: rule__Instanciation__Group__2__Impl rule__Instanciation__Group__3
            {
            pushFollow(FOLLOW_rule__Instanciation__Group__2__Impl_in_rule__Instanciation__Group__21963);
            rule__Instanciation__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Instanciation__Group__3_in_rule__Instanciation__Group__21966);
            rule__Instanciation__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instanciation__Group__2"


    // $ANTLR start "rule__Instanciation__Group__2__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:988:1: rule__Instanciation__Group__2__Impl : ( '=' ) ;
    public final void rule__Instanciation__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:992:1: ( ( '=' ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:993:1: ( '=' )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:993:1: ( '=' )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:994:1: '='
            {
             before(grammarAccess.getInstanciationAccess().getEqualsSignKeyword_2()); 
            match(input,18,FOLLOW_18_in_rule__Instanciation__Group__2__Impl1994); 
             after(grammarAccess.getInstanciationAccess().getEqualsSignKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instanciation__Group__2__Impl"


    // $ANTLR start "rule__Instanciation__Group__3"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1007:1: rule__Instanciation__Group__3 : rule__Instanciation__Group__3__Impl rule__Instanciation__Group__4 ;
    public final void rule__Instanciation__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1011:1: ( rule__Instanciation__Group__3__Impl rule__Instanciation__Group__4 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1012:2: rule__Instanciation__Group__3__Impl rule__Instanciation__Group__4
            {
            pushFollow(FOLLOW_rule__Instanciation__Group__3__Impl_in_rule__Instanciation__Group__32025);
            rule__Instanciation__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Instanciation__Group__4_in_rule__Instanciation__Group__32028);
            rule__Instanciation__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instanciation__Group__3"


    // $ANTLR start "rule__Instanciation__Group__3__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1019:1: rule__Instanciation__Group__3__Impl : ( ( rule__Instanciation__VariableAssignment_3 ) ) ;
    public final void rule__Instanciation__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1023:1: ( ( ( rule__Instanciation__VariableAssignment_3 ) ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1024:1: ( ( rule__Instanciation__VariableAssignment_3 ) )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1024:1: ( ( rule__Instanciation__VariableAssignment_3 ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1025:1: ( rule__Instanciation__VariableAssignment_3 )
            {
             before(grammarAccess.getInstanciationAccess().getVariableAssignment_3()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1026:1: ( rule__Instanciation__VariableAssignment_3 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1026:2: rule__Instanciation__VariableAssignment_3
            {
            pushFollow(FOLLOW_rule__Instanciation__VariableAssignment_3_in_rule__Instanciation__Group__3__Impl2055);
            rule__Instanciation__VariableAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getInstanciationAccess().getVariableAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instanciation__Group__3__Impl"


    // $ANTLR start "rule__Instanciation__Group__4"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1036:1: rule__Instanciation__Group__4 : rule__Instanciation__Group__4__Impl ;
    public final void rule__Instanciation__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1040:1: ( rule__Instanciation__Group__4__Impl )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1041:2: rule__Instanciation__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__Instanciation__Group__4__Impl_in_rule__Instanciation__Group__42085);
            rule__Instanciation__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instanciation__Group__4"


    // $ANTLR start "rule__Instanciation__Group__4__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1047:1: rule__Instanciation__Group__4__Impl : ( ';' ) ;
    public final void rule__Instanciation__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1051:1: ( ( ';' ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1052:1: ( ';' )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1052:1: ( ';' )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1053:1: ';'
            {
             before(grammarAccess.getInstanciationAccess().getSemicolonKeyword_4()); 
            match(input,17,FOLLOW_17_in_rule__Instanciation__Group__4__Impl2113); 
             after(grammarAccess.getInstanciationAccess().getSemicolonKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instanciation__Group__4__Impl"


    // $ANTLR start "rule__Addition__Group__0"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1076:1: rule__Addition__Group__0 : rule__Addition__Group__0__Impl rule__Addition__Group__1 ;
    public final void rule__Addition__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1080:1: ( rule__Addition__Group__0__Impl rule__Addition__Group__1 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1081:2: rule__Addition__Group__0__Impl rule__Addition__Group__1
            {
            pushFollow(FOLLOW_rule__Addition__Group__0__Impl_in_rule__Addition__Group__02154);
            rule__Addition__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Addition__Group__1_in_rule__Addition__Group__02157);
            rule__Addition__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__Group__0"


    // $ANTLR start "rule__Addition__Group__0__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1088:1: rule__Addition__Group__0__Impl : ( ( 'var' )? ) ;
    public final void rule__Addition__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1092:1: ( ( ( 'var' )? ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1093:1: ( ( 'var' )? )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1093:1: ( ( 'var' )? )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1094:1: ( 'var' )?
            {
             before(grammarAccess.getAdditionAccess().getVarKeyword_0()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1095:1: ( 'var' )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==16) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1096:2: 'var'
                    {
                    match(input,16,FOLLOW_16_in_rule__Addition__Group__0__Impl2186); 

                    }
                    break;

            }

             after(grammarAccess.getAdditionAccess().getVarKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__Group__0__Impl"


    // $ANTLR start "rule__Addition__Group__1"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1107:1: rule__Addition__Group__1 : rule__Addition__Group__1__Impl rule__Addition__Group__2 ;
    public final void rule__Addition__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1111:1: ( rule__Addition__Group__1__Impl rule__Addition__Group__2 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1112:2: rule__Addition__Group__1__Impl rule__Addition__Group__2
            {
            pushFollow(FOLLOW_rule__Addition__Group__1__Impl_in_rule__Addition__Group__12219);
            rule__Addition__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Addition__Group__2_in_rule__Addition__Group__12222);
            rule__Addition__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__Group__1"


    // $ANTLR start "rule__Addition__Group__1__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1119:1: rule__Addition__Group__1__Impl : ( ( rule__Addition__NameAssignment_1 ) ) ;
    public final void rule__Addition__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1123:1: ( ( ( rule__Addition__NameAssignment_1 ) ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1124:1: ( ( rule__Addition__NameAssignment_1 ) )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1124:1: ( ( rule__Addition__NameAssignment_1 ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1125:1: ( rule__Addition__NameAssignment_1 )
            {
             before(grammarAccess.getAdditionAccess().getNameAssignment_1()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1126:1: ( rule__Addition__NameAssignment_1 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1126:2: rule__Addition__NameAssignment_1
            {
            pushFollow(FOLLOW_rule__Addition__NameAssignment_1_in_rule__Addition__Group__1__Impl2249);
            rule__Addition__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getAdditionAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__Group__1__Impl"


    // $ANTLR start "rule__Addition__Group__2"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1136:1: rule__Addition__Group__2 : rule__Addition__Group__2__Impl rule__Addition__Group__3 ;
    public final void rule__Addition__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1140:1: ( rule__Addition__Group__2__Impl rule__Addition__Group__3 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1141:2: rule__Addition__Group__2__Impl rule__Addition__Group__3
            {
            pushFollow(FOLLOW_rule__Addition__Group__2__Impl_in_rule__Addition__Group__22279);
            rule__Addition__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Addition__Group__3_in_rule__Addition__Group__22282);
            rule__Addition__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__Group__2"


    // $ANTLR start "rule__Addition__Group__2__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1148:1: rule__Addition__Group__2__Impl : ( '=' ) ;
    public final void rule__Addition__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1152:1: ( ( '=' ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1153:1: ( '=' )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1153:1: ( '=' )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1154:1: '='
            {
             before(grammarAccess.getAdditionAccess().getEqualsSignKeyword_2()); 
            match(input,18,FOLLOW_18_in_rule__Addition__Group__2__Impl2310); 
             after(grammarAccess.getAdditionAccess().getEqualsSignKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__Group__2__Impl"


    // $ANTLR start "rule__Addition__Group__3"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1167:1: rule__Addition__Group__3 : rule__Addition__Group__3__Impl rule__Addition__Group__4 ;
    public final void rule__Addition__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1171:1: ( rule__Addition__Group__3__Impl rule__Addition__Group__4 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1172:2: rule__Addition__Group__3__Impl rule__Addition__Group__4
            {
            pushFollow(FOLLOW_rule__Addition__Group__3__Impl_in_rule__Addition__Group__32341);
            rule__Addition__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Addition__Group__4_in_rule__Addition__Group__32344);
            rule__Addition__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__Group__3"


    // $ANTLR start "rule__Addition__Group__3__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1179:1: rule__Addition__Group__3__Impl : ( ( rule__Addition__Variable1Assignment_3 ) ) ;
    public final void rule__Addition__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1183:1: ( ( ( rule__Addition__Variable1Assignment_3 ) ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1184:1: ( ( rule__Addition__Variable1Assignment_3 ) )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1184:1: ( ( rule__Addition__Variable1Assignment_3 ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1185:1: ( rule__Addition__Variable1Assignment_3 )
            {
             before(grammarAccess.getAdditionAccess().getVariable1Assignment_3()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1186:1: ( rule__Addition__Variable1Assignment_3 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1186:2: rule__Addition__Variable1Assignment_3
            {
            pushFollow(FOLLOW_rule__Addition__Variable1Assignment_3_in_rule__Addition__Group__3__Impl2371);
            rule__Addition__Variable1Assignment_3();

            state._fsp--;


            }

             after(grammarAccess.getAdditionAccess().getVariable1Assignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__Group__3__Impl"


    // $ANTLR start "rule__Addition__Group__4"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1196:1: rule__Addition__Group__4 : rule__Addition__Group__4__Impl rule__Addition__Group__5 ;
    public final void rule__Addition__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1200:1: ( rule__Addition__Group__4__Impl rule__Addition__Group__5 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1201:2: rule__Addition__Group__4__Impl rule__Addition__Group__5
            {
            pushFollow(FOLLOW_rule__Addition__Group__4__Impl_in_rule__Addition__Group__42401);
            rule__Addition__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Addition__Group__5_in_rule__Addition__Group__42404);
            rule__Addition__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__Group__4"


    // $ANTLR start "rule__Addition__Group__4__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1208:1: rule__Addition__Group__4__Impl : ( '+' ) ;
    public final void rule__Addition__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1212:1: ( ( '+' ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1213:1: ( '+' )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1213:1: ( '+' )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1214:1: '+'
            {
             before(grammarAccess.getAdditionAccess().getPlusSignKeyword_4()); 
            match(input,19,FOLLOW_19_in_rule__Addition__Group__4__Impl2432); 
             after(grammarAccess.getAdditionAccess().getPlusSignKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__Group__4__Impl"


    // $ANTLR start "rule__Addition__Group__5"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1227:1: rule__Addition__Group__5 : rule__Addition__Group__5__Impl rule__Addition__Group__6 ;
    public final void rule__Addition__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1231:1: ( rule__Addition__Group__5__Impl rule__Addition__Group__6 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1232:2: rule__Addition__Group__5__Impl rule__Addition__Group__6
            {
            pushFollow(FOLLOW_rule__Addition__Group__5__Impl_in_rule__Addition__Group__52463);
            rule__Addition__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Addition__Group__6_in_rule__Addition__Group__52466);
            rule__Addition__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__Group__5"


    // $ANTLR start "rule__Addition__Group__5__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1239:1: rule__Addition__Group__5__Impl : ( ( rule__Addition__Variable2Assignment_5 ) ) ;
    public final void rule__Addition__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1243:1: ( ( ( rule__Addition__Variable2Assignment_5 ) ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1244:1: ( ( rule__Addition__Variable2Assignment_5 ) )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1244:1: ( ( rule__Addition__Variable2Assignment_5 ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1245:1: ( rule__Addition__Variable2Assignment_5 )
            {
             before(grammarAccess.getAdditionAccess().getVariable2Assignment_5()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1246:1: ( rule__Addition__Variable2Assignment_5 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1246:2: rule__Addition__Variable2Assignment_5
            {
            pushFollow(FOLLOW_rule__Addition__Variable2Assignment_5_in_rule__Addition__Group__5__Impl2493);
            rule__Addition__Variable2Assignment_5();

            state._fsp--;


            }

             after(grammarAccess.getAdditionAccess().getVariable2Assignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__Group__5__Impl"


    // $ANTLR start "rule__Addition__Group__6"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1256:1: rule__Addition__Group__6 : rule__Addition__Group__6__Impl ;
    public final void rule__Addition__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1260:1: ( rule__Addition__Group__6__Impl )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1261:2: rule__Addition__Group__6__Impl
            {
            pushFollow(FOLLOW_rule__Addition__Group__6__Impl_in_rule__Addition__Group__62523);
            rule__Addition__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__Group__6"


    // $ANTLR start "rule__Addition__Group__6__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1267:1: rule__Addition__Group__6__Impl : ( ';' ) ;
    public final void rule__Addition__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1271:1: ( ( ';' ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1272:1: ( ';' )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1272:1: ( ';' )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1273:1: ';'
            {
             before(grammarAccess.getAdditionAccess().getSemicolonKeyword_6()); 
            match(input,17,FOLLOW_17_in_rule__Addition__Group__6__Impl2551); 
             after(grammarAccess.getAdditionAccess().getSemicolonKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__Group__6__Impl"


    // $ANTLR start "rule__Alert__Group__0"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1300:1: rule__Alert__Group__0 : rule__Alert__Group__0__Impl rule__Alert__Group__1 ;
    public final void rule__Alert__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1304:1: ( rule__Alert__Group__0__Impl rule__Alert__Group__1 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1305:2: rule__Alert__Group__0__Impl rule__Alert__Group__1
            {
            pushFollow(FOLLOW_rule__Alert__Group__0__Impl_in_rule__Alert__Group__02596);
            rule__Alert__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Alert__Group__1_in_rule__Alert__Group__02599);
            rule__Alert__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alert__Group__0"


    // $ANTLR start "rule__Alert__Group__0__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1312:1: rule__Alert__Group__0__Impl : ( 'alert(\"' ) ;
    public final void rule__Alert__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1316:1: ( ( 'alert(\"' ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1317:1: ( 'alert(\"' )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1317:1: ( 'alert(\"' )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1318:1: 'alert(\"'
            {
             before(grammarAccess.getAlertAccess().getAlertKeyword_0()); 
            match(input,20,FOLLOW_20_in_rule__Alert__Group__0__Impl2627); 
             after(grammarAccess.getAlertAccess().getAlertKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alert__Group__0__Impl"


    // $ANTLR start "rule__Alert__Group__1"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1331:1: rule__Alert__Group__1 : rule__Alert__Group__1__Impl rule__Alert__Group__2 ;
    public final void rule__Alert__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1335:1: ( rule__Alert__Group__1__Impl rule__Alert__Group__2 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1336:2: rule__Alert__Group__1__Impl rule__Alert__Group__2
            {
            pushFollow(FOLLOW_rule__Alert__Group__1__Impl_in_rule__Alert__Group__12658);
            rule__Alert__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Alert__Group__2_in_rule__Alert__Group__12661);
            rule__Alert__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alert__Group__1"


    // $ANTLR start "rule__Alert__Group__1__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1343:1: rule__Alert__Group__1__Impl : ( ( rule__Alert__MessageAssignment_1 ) ) ;
    public final void rule__Alert__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1347:1: ( ( ( rule__Alert__MessageAssignment_1 ) ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1348:1: ( ( rule__Alert__MessageAssignment_1 ) )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1348:1: ( ( rule__Alert__MessageAssignment_1 ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1349:1: ( rule__Alert__MessageAssignment_1 )
            {
             before(grammarAccess.getAlertAccess().getMessageAssignment_1()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1350:1: ( rule__Alert__MessageAssignment_1 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1350:2: rule__Alert__MessageAssignment_1
            {
            pushFollow(FOLLOW_rule__Alert__MessageAssignment_1_in_rule__Alert__Group__1__Impl2688);
            rule__Alert__MessageAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getAlertAccess().getMessageAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alert__Group__1__Impl"


    // $ANTLR start "rule__Alert__Group__2"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1360:1: rule__Alert__Group__2 : rule__Alert__Group__2__Impl ;
    public final void rule__Alert__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1364:1: ( rule__Alert__Group__2__Impl )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1365:2: rule__Alert__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Alert__Group__2__Impl_in_rule__Alert__Group__22718);
            rule__Alert__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alert__Group__2"


    // $ANTLR start "rule__Alert__Group__2__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1371:1: rule__Alert__Group__2__Impl : ( '\");' ) ;
    public final void rule__Alert__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1375:1: ( ( '\");' ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1376:1: ( '\");' )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1376:1: ( '\");' )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1377:1: '\");'
            {
             before(grammarAccess.getAlertAccess().getQuotationMarkRightParenthesisSemicolonKeyword_2()); 
            match(input,21,FOLLOW_21_in_rule__Alert__Group__2__Impl2746); 
             after(grammarAccess.getAlertAccess().getQuotationMarkRightParenthesisSemicolonKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alert__Group__2__Impl"


    // $ANTLR start "rule__Prompt__Group__0"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1396:1: rule__Prompt__Group__0 : rule__Prompt__Group__0__Impl rule__Prompt__Group__1 ;
    public final void rule__Prompt__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1400:1: ( rule__Prompt__Group__0__Impl rule__Prompt__Group__1 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1401:2: rule__Prompt__Group__0__Impl rule__Prompt__Group__1
            {
            pushFollow(FOLLOW_rule__Prompt__Group__0__Impl_in_rule__Prompt__Group__02783);
            rule__Prompt__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Prompt__Group__1_in_rule__Prompt__Group__02786);
            rule__Prompt__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prompt__Group__0"


    // $ANTLR start "rule__Prompt__Group__0__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1408:1: rule__Prompt__Group__0__Impl : ( 'prompt(\"' ) ;
    public final void rule__Prompt__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1412:1: ( ( 'prompt(\"' ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1413:1: ( 'prompt(\"' )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1413:1: ( 'prompt(\"' )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1414:1: 'prompt(\"'
            {
             before(grammarAccess.getPromptAccess().getPromptKeyword_0()); 
            match(input,22,FOLLOW_22_in_rule__Prompt__Group__0__Impl2814); 
             after(grammarAccess.getPromptAccess().getPromptKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prompt__Group__0__Impl"


    // $ANTLR start "rule__Prompt__Group__1"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1427:1: rule__Prompt__Group__1 : rule__Prompt__Group__1__Impl rule__Prompt__Group__2 ;
    public final void rule__Prompt__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1431:1: ( rule__Prompt__Group__1__Impl rule__Prompt__Group__2 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1432:2: rule__Prompt__Group__1__Impl rule__Prompt__Group__2
            {
            pushFollow(FOLLOW_rule__Prompt__Group__1__Impl_in_rule__Prompt__Group__12845);
            rule__Prompt__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Prompt__Group__2_in_rule__Prompt__Group__12848);
            rule__Prompt__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prompt__Group__1"


    // $ANTLR start "rule__Prompt__Group__1__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1439:1: rule__Prompt__Group__1__Impl : ( ( rule__Prompt__Votre_textAssignment_1 ) ) ;
    public final void rule__Prompt__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1443:1: ( ( ( rule__Prompt__Votre_textAssignment_1 ) ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1444:1: ( ( rule__Prompt__Votre_textAssignment_1 ) )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1444:1: ( ( rule__Prompt__Votre_textAssignment_1 ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1445:1: ( rule__Prompt__Votre_textAssignment_1 )
            {
             before(grammarAccess.getPromptAccess().getVotre_textAssignment_1()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1446:1: ( rule__Prompt__Votre_textAssignment_1 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1446:2: rule__Prompt__Votre_textAssignment_1
            {
            pushFollow(FOLLOW_rule__Prompt__Votre_textAssignment_1_in_rule__Prompt__Group__1__Impl2875);
            rule__Prompt__Votre_textAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getPromptAccess().getVotre_textAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prompt__Group__1__Impl"


    // $ANTLR start "rule__Prompt__Group__2"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1456:1: rule__Prompt__Group__2 : rule__Prompt__Group__2__Impl rule__Prompt__Group__3 ;
    public final void rule__Prompt__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1460:1: ( rule__Prompt__Group__2__Impl rule__Prompt__Group__3 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1461:2: rule__Prompt__Group__2__Impl rule__Prompt__Group__3
            {
            pushFollow(FOLLOW_rule__Prompt__Group__2__Impl_in_rule__Prompt__Group__22905);
            rule__Prompt__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Prompt__Group__3_in_rule__Prompt__Group__22908);
            rule__Prompt__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prompt__Group__2"


    // $ANTLR start "rule__Prompt__Group__2__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1468:1: rule__Prompt__Group__2__Impl : ( ',' ) ;
    public final void rule__Prompt__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1472:1: ( ( ',' ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1473:1: ( ',' )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1473:1: ( ',' )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1474:1: ','
            {
             before(grammarAccess.getPromptAccess().getCommaKeyword_2()); 
            match(input,15,FOLLOW_15_in_rule__Prompt__Group__2__Impl2936); 
             after(grammarAccess.getPromptAccess().getCommaKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prompt__Group__2__Impl"


    // $ANTLR start "rule__Prompt__Group__3"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1487:1: rule__Prompt__Group__3 : rule__Prompt__Group__3__Impl rule__Prompt__Group__4 ;
    public final void rule__Prompt__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1491:1: ( rule__Prompt__Group__3__Impl rule__Prompt__Group__4 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1492:2: rule__Prompt__Group__3__Impl rule__Prompt__Group__4
            {
            pushFollow(FOLLOW_rule__Prompt__Group__3__Impl_in_rule__Prompt__Group__32967);
            rule__Prompt__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Prompt__Group__4_in_rule__Prompt__Group__32970);
            rule__Prompt__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prompt__Group__3"


    // $ANTLR start "rule__Prompt__Group__3__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1499:1: rule__Prompt__Group__3__Impl : ( ( rule__Prompt__Text_par_defautAssignment_3 ) ) ;
    public final void rule__Prompt__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1503:1: ( ( ( rule__Prompt__Text_par_defautAssignment_3 ) ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1504:1: ( ( rule__Prompt__Text_par_defautAssignment_3 ) )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1504:1: ( ( rule__Prompt__Text_par_defautAssignment_3 ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1505:1: ( rule__Prompt__Text_par_defautAssignment_3 )
            {
             before(grammarAccess.getPromptAccess().getText_par_defautAssignment_3()); 
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1506:1: ( rule__Prompt__Text_par_defautAssignment_3 )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1506:2: rule__Prompt__Text_par_defautAssignment_3
            {
            pushFollow(FOLLOW_rule__Prompt__Text_par_defautAssignment_3_in_rule__Prompt__Group__3__Impl2997);
            rule__Prompt__Text_par_defautAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getPromptAccess().getText_par_defautAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prompt__Group__3__Impl"


    // $ANTLR start "rule__Prompt__Group__4"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1516:1: rule__Prompt__Group__4 : rule__Prompt__Group__4__Impl ;
    public final void rule__Prompt__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1520:1: ( rule__Prompt__Group__4__Impl )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1521:2: rule__Prompt__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__Prompt__Group__4__Impl_in_rule__Prompt__Group__43027);
            rule__Prompt__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prompt__Group__4"


    // $ANTLR start "rule__Prompt__Group__4__Impl"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1527:1: rule__Prompt__Group__4__Impl : ( '\");' ) ;
    public final void rule__Prompt__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1531:1: ( ( '\");' ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1532:1: ( '\");' )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1532:1: ( '\");' )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1533:1: '\");'
            {
             before(grammarAccess.getPromptAccess().getQuotationMarkRightParenthesisSemicolonKeyword_4()); 
            match(input,21,FOLLOW_21_in_rule__Prompt__Group__4__Impl3055); 
             after(grammarAccess.getPromptAccess().getQuotationMarkRightParenthesisSemicolonKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prompt__Group__4__Impl"


    // $ANTLR start "rule__Domainmodel__ElementsAssignment"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1557:1: rule__Domainmodel__ElementsAssignment : ( ruleAbstractElement ) ;
    public final void rule__Domainmodel__ElementsAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1561:1: ( ( ruleAbstractElement ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1562:1: ( ruleAbstractElement )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1562:1: ( ruleAbstractElement )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1563:1: ruleAbstractElement
            {
             before(grammarAccess.getDomainmodelAccess().getElementsAbstractElementParserRuleCall_0()); 
            pushFollow(FOLLOW_ruleAbstractElement_in_rule__Domainmodel__ElementsAssignment3101);
            ruleAbstractElement();

            state._fsp--;

             after(grammarAccess.getDomainmodelAccess().getElementsAbstractElementParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Domainmodel__ElementsAssignment"


    // $ANTLR start "rule__Declaration_de_fonction__FunctionNameAssignment_1"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1572:1: rule__Declaration_de_fonction__FunctionNameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Declaration_de_fonction__FunctionNameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1576:1: ( ( RULE_ID ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1577:1: ( RULE_ID )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1577:1: ( RULE_ID )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1578:1: RULE_ID
            {
             before(grammarAccess.getDeclaration_de_fonctionAccess().getFunctionNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Declaration_de_fonction__FunctionNameAssignment_13132); 
             after(grammarAccess.getDeclaration_de_fonctionAccess().getFunctionNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_de_fonction__FunctionNameAssignment_1"


    // $ANTLR start "rule__Declaration_de_fonction__ArgumentsAssignment_3"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1587:1: rule__Declaration_de_fonction__ArgumentsAssignment_3 : ( RULE_ID ) ;
    public final void rule__Declaration_de_fonction__ArgumentsAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1591:1: ( ( RULE_ID ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1592:1: ( RULE_ID )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1592:1: ( RULE_ID )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1593:1: RULE_ID
            {
             before(grammarAccess.getDeclaration_de_fonctionAccess().getArgumentsIDTerminalRuleCall_3_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Declaration_de_fonction__ArgumentsAssignment_33163); 
             after(grammarAccess.getDeclaration_de_fonctionAccess().getArgumentsIDTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_de_fonction__ArgumentsAssignment_3"


    // $ANTLR start "rule__Declaration_de_fonction__ArgumentAssignment_4_1"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1602:1: rule__Declaration_de_fonction__ArgumentAssignment_4_1 : ( RULE_ID ) ;
    public final void rule__Declaration_de_fonction__ArgumentAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1606:1: ( ( RULE_ID ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1607:1: ( RULE_ID )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1607:1: ( RULE_ID )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1608:1: RULE_ID
            {
             before(grammarAccess.getDeclaration_de_fonctionAccess().getArgumentIDTerminalRuleCall_4_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Declaration_de_fonction__ArgumentAssignment_4_13194); 
             after(grammarAccess.getDeclaration_de_fonctionAccess().getArgumentIDTerminalRuleCall_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_de_fonction__ArgumentAssignment_4_1"


    // $ANTLR start "rule__Declaration_de_fonction__ElementAssignment_6"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1617:1: rule__Declaration_de_fonction__ElementAssignment_6 : ( rulefunctionContent ) ;
    public final void rule__Declaration_de_fonction__ElementAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1621:1: ( ( rulefunctionContent ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1622:1: ( rulefunctionContent )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1622:1: ( rulefunctionContent )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1623:1: rulefunctionContent
            {
             before(grammarAccess.getDeclaration_de_fonctionAccess().getElementFunctionContentParserRuleCall_6_0()); 
            pushFollow(FOLLOW_rulefunctionContent_in_rule__Declaration_de_fonction__ElementAssignment_63225);
            rulefunctionContent();

            state._fsp--;

             after(grammarAccess.getDeclaration_de_fonctionAccess().getElementFunctionContentParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Declaration_de_fonction__ElementAssignment_6"


    // $ANTLR start "rule__Type__NameAssignment_1"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1632:1: rule__Type__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Type__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1636:1: ( ( RULE_ID ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1637:1: ( RULE_ID )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1637:1: ( RULE_ID )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1638:1: RULE_ID
            {
             before(grammarAccess.getTypeAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Type__NameAssignment_13256); 
             after(grammarAccess.getTypeAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__NameAssignment_1"


    // $ANTLR start "rule__Instructions__NameAssignment_4_0"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1647:1: rule__Instructions__NameAssignment_4_0 : ( RULE_ID ) ;
    public final void rule__Instructions__NameAssignment_4_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1651:1: ( ( RULE_ID ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1652:1: ( RULE_ID )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1652:1: ( RULE_ID )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1653:1: RULE_ID
            {
             before(grammarAccess.getInstructionsAccess().getNameIDTerminalRuleCall_4_0_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Instructions__NameAssignment_4_03287); 
             after(grammarAccess.getInstructionsAccess().getNameIDTerminalRuleCall_4_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instructions__NameAssignment_4_0"


    // $ANTLR start "rule__Instanciation__NameAssignment_1"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1662:1: rule__Instanciation__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Instanciation__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1666:1: ( ( RULE_ID ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1667:1: ( RULE_ID )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1667:1: ( RULE_ID )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1668:1: RULE_ID
            {
             before(grammarAccess.getInstanciationAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Instanciation__NameAssignment_13318); 
             after(grammarAccess.getInstanciationAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instanciation__NameAssignment_1"


    // $ANTLR start "rule__Instanciation__VariableAssignment_3"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1677:1: rule__Instanciation__VariableAssignment_3 : ( RULE_ID ) ;
    public final void rule__Instanciation__VariableAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1681:1: ( ( RULE_ID ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1682:1: ( RULE_ID )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1682:1: ( RULE_ID )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1683:1: RULE_ID
            {
             before(grammarAccess.getInstanciationAccess().getVariableIDTerminalRuleCall_3_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Instanciation__VariableAssignment_33349); 
             after(grammarAccess.getInstanciationAccess().getVariableIDTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Instanciation__VariableAssignment_3"


    // $ANTLR start "rule__Addition__NameAssignment_1"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1692:1: rule__Addition__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Addition__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1696:1: ( ( RULE_ID ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1697:1: ( RULE_ID )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1697:1: ( RULE_ID )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1698:1: RULE_ID
            {
             before(grammarAccess.getAdditionAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Addition__NameAssignment_13380); 
             after(grammarAccess.getAdditionAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__NameAssignment_1"


    // $ANTLR start "rule__Addition__Variable1Assignment_3"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1707:1: rule__Addition__Variable1Assignment_3 : ( RULE_ID ) ;
    public final void rule__Addition__Variable1Assignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1711:1: ( ( RULE_ID ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1712:1: ( RULE_ID )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1712:1: ( RULE_ID )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1713:1: RULE_ID
            {
             before(grammarAccess.getAdditionAccess().getVariable1IDTerminalRuleCall_3_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Addition__Variable1Assignment_33411); 
             after(grammarAccess.getAdditionAccess().getVariable1IDTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__Variable1Assignment_3"


    // $ANTLR start "rule__Addition__Variable2Assignment_5"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1722:1: rule__Addition__Variable2Assignment_5 : ( RULE_ID ) ;
    public final void rule__Addition__Variable2Assignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1726:1: ( ( RULE_ID ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1727:1: ( RULE_ID )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1727:1: ( RULE_ID )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1728:1: RULE_ID
            {
             before(grammarAccess.getAdditionAccess().getVariable2IDTerminalRuleCall_5_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Addition__Variable2Assignment_53442); 
             after(grammarAccess.getAdditionAccess().getVariable2IDTerminalRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Addition__Variable2Assignment_5"


    // $ANTLR start "rule__Alert__MessageAssignment_1"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1737:1: rule__Alert__MessageAssignment_1 : ( RULE_ID ) ;
    public final void rule__Alert__MessageAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1741:1: ( ( RULE_ID ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1742:1: ( RULE_ID )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1742:1: ( RULE_ID )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1743:1: RULE_ID
            {
             before(grammarAccess.getAlertAccess().getMessageIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Alert__MessageAssignment_13473); 
             after(grammarAccess.getAlertAccess().getMessageIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Alert__MessageAssignment_1"


    // $ANTLR start "rule__Prompt__Votre_textAssignment_1"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1752:1: rule__Prompt__Votre_textAssignment_1 : ( RULE_ID ) ;
    public final void rule__Prompt__Votre_textAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1756:1: ( ( RULE_ID ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1757:1: ( RULE_ID )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1757:1: ( RULE_ID )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1758:1: RULE_ID
            {
             before(grammarAccess.getPromptAccess().getVotre_textIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Prompt__Votre_textAssignment_13504); 
             after(grammarAccess.getPromptAccess().getVotre_textIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prompt__Votre_textAssignment_1"


    // $ANTLR start "rule__Prompt__Text_par_defautAssignment_3"
    // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1767:1: rule__Prompt__Text_par_defautAssignment_3 : ( RULE_ID ) ;
    public final void rule__Prompt__Text_par_defautAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1771:1: ( ( RULE_ID ) )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1772:1: ( RULE_ID )
            {
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1772:1: ( RULE_ID )
            // ../org.xtext.editorforjs.ui/src-gen/org/xtext/ui/contentassist/antlr/internal/InternalEditorForJS.g:1773:1: RULE_ID
            {
             before(grammarAccess.getPromptAccess().getText_par_defautIDTerminalRuleCall_3_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Prompt__Text_par_defautAssignment_33535); 
             after(grammarAccess.getPromptAccess().getText_par_defautIDTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Prompt__Text_par_defautAssignment_3"

    // Delegated rules


    protected DFA4 dfa4 = new DFA4(this);
    static final String DFA4_eotS =
        "\13\uffff";
    static final String DFA4_eofS =
        "\13\uffff";
    static final String DFA4_minS =
        "\2\4\1\21\2\uffff\1\22\1\4\1\uffff\1\21\2\uffff";
    static final String DFA4_maxS =
        "\1\26\1\4\1\22\2\uffff\1\22\1\4\1\uffff\1\23\2\uffff";
    static final String DFA4_acceptS =
        "\3\uffff\1\3\1\4\2\uffff\1\5\1\uffff\1\1\1\2";
    static final String DFA4_specialS =
        "\13\uffff}>";
    static final String[] DFA4_transitionS = {
            "\1\2\13\uffff\1\1\3\uffff\1\3\1\uffff\1\4",
            "\1\5",
            "\1\7\1\6",
            "",
            "",
            "\1\6",
            "\1\10",
            "",
            "\1\11\1\uffff\1\12",
            "",
            ""
    };

    static final short[] DFA4_eot = DFA.unpackEncodedString(DFA4_eotS);
    static final short[] DFA4_eof = DFA.unpackEncodedString(DFA4_eofS);
    static final char[] DFA4_min = DFA.unpackEncodedStringToUnsignedChars(DFA4_minS);
    static final char[] DFA4_max = DFA.unpackEncodedStringToUnsignedChars(DFA4_maxS);
    static final short[] DFA4_accept = DFA.unpackEncodedString(DFA4_acceptS);
    static final short[] DFA4_special = DFA.unpackEncodedString(DFA4_specialS);
    static final short[][] DFA4_transition;

    static {
        int numStates = DFA4_transitionS.length;
        DFA4_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA4_transition[i] = DFA.unpackEncodedString(DFA4_transitionS[i]);
        }
    }

    class DFA4 extends DFA {

        public DFA4(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 4;
            this.eot = DFA4_eot;
            this.eof = DFA4_eof;
            this.min = DFA4_min;
            this.max = DFA4_max;
            this.accept = DFA4_accept;
            this.special = DFA4_special;
            this.transition = DFA4_transition;
        }
        public String getDescription() {
            return "390:1: rule__Instructions__Alternatives : ( ( ruleinstanciation ) | ( ruleaddition ) | ( rulealert ) | ( ruleprompt ) | ( ( rule__Instructions__Group_4__0 ) ) );";
        }
    }
 

    public static final BitSet FOLLOW_ruleDomainmodel_in_entryRuleDomainmodel61 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDomainmodel68 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Domainmodel__ElementsAssignment_in_ruleDomainmodel94 = new BitSet(new long[]{0x0000000000510812L});
    public static final BitSet FOLLOW_ruleAbstractElement_in_entryRuleAbstractElement122 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleAbstractElement129 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__AbstractElement__Alternatives_in_ruleAbstractElement155 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDeclaration_de_fonction_in_entryRuleDeclaration_de_fonction182 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleDeclaration_de_fonction189 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Declaration_de_fonction__Group__0_in_ruleDeclaration_de_fonction215 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulefunctionContent_in_entryRulefunctionContent242 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulefunctionContent249 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__FunctionContent__Alternatives_in_rulefunctionContent275 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleType_in_entryRuleType302 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleType309 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Type__Group__0_in_ruleType335 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInstructions_in_entryRuleInstructions362 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleInstructions369 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Instructions__Alternatives_in_ruleInstructions395 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleinstanciation_in_entryRuleinstanciation422 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleinstanciation429 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Instanciation__Group__0_in_ruleinstanciation455 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleaddition_in_entryRuleaddition482 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleaddition489 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Addition__Group__0_in_ruleaddition515 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulealert_in_entryRulealert542 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulealert549 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Alert__Group__0_in_rulealert575 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleprompt_in_entryRuleprompt602 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleprompt609 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Prompt__Group__0_in_ruleprompt635 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleDeclaration_de_fonction_in_rule__AbstractElement__Alternatives671 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleType_in_rule__AbstractElement__Alternatives688 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInstructions_in_rule__AbstractElement__Alternatives705 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleInstructions_in_rule__FunctionContent__Alternatives737 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleType_in_rule__FunctionContent__Alternatives754 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleinstanciation_in_rule__Instructions__Alternatives786 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleaddition_in_rule__Instructions__Alternatives803 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulealert_in_rule__Instructions__Alternatives820 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleprompt_in_rule__Instructions__Alternatives837 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Instructions__Group_4__0_in_rule__Instructions__Alternatives854 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Declaration_de_fonction__Group__0__Impl_in_rule__Declaration_de_fonction__Group__0885 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Declaration_de_fonction__Group__1_in_rule__Declaration_de_fonction__Group__0888 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rule__Declaration_de_fonction__Group__0__Impl916 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Declaration_de_fonction__Group__1__Impl_in_rule__Declaration_de_fonction__Group__1947 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_rule__Declaration_de_fonction__Group__2_in_rule__Declaration_de_fonction__Group__1950 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Declaration_de_fonction__FunctionNameAssignment_1_in_rule__Declaration_de_fonction__Group__1__Impl977 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Declaration_de_fonction__Group__2__Impl_in_rule__Declaration_de_fonction__Group__21007 = new BitSet(new long[]{0x000000000000A010L});
    public static final BitSet FOLLOW_rule__Declaration_de_fonction__Group__3_in_rule__Declaration_de_fonction__Group__21010 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__Declaration_de_fonction__Group__2__Impl1038 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Declaration_de_fonction__Group__3__Impl_in_rule__Declaration_de_fonction__Group__31069 = new BitSet(new long[]{0x000000000000A010L});
    public static final BitSet FOLLOW_rule__Declaration_de_fonction__Group__4_in_rule__Declaration_de_fonction__Group__31072 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Declaration_de_fonction__ArgumentsAssignment_3_in_rule__Declaration_de_fonction__Group__3__Impl1099 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Declaration_de_fonction__Group__4__Impl_in_rule__Declaration_de_fonction__Group__41130 = new BitSet(new long[]{0x000000000000A010L});
    public static final BitSet FOLLOW_rule__Declaration_de_fonction__Group__5_in_rule__Declaration_de_fonction__Group__41133 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Declaration_de_fonction__Group_4__0_in_rule__Declaration_de_fonction__Group__4__Impl1160 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_rule__Declaration_de_fonction__Group__5__Impl_in_rule__Declaration_de_fonction__Group__51191 = new BitSet(new long[]{0x0000000000510810L});
    public static final BitSet FOLLOW_rule__Declaration_de_fonction__Group__6_in_rule__Declaration_de_fonction__Group__51194 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__Declaration_de_fonction__Group__5__Impl1222 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Declaration_de_fonction__Group__6__Impl_in_rule__Declaration_de_fonction__Group__61253 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_rule__Declaration_de_fonction__Group__7_in_rule__Declaration_de_fonction__Group__61256 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Declaration_de_fonction__ElementAssignment_6_in_rule__Declaration_de_fonction__Group__6__Impl1285 = new BitSet(new long[]{0x0000000000510812L});
    public static final BitSet FOLLOW_rule__Declaration_de_fonction__ElementAssignment_6_in_rule__Declaration_de_fonction__Group__6__Impl1297 = new BitSet(new long[]{0x0000000000510812L});
    public static final BitSet FOLLOW_rule__Declaration_de_fonction__Group__7__Impl_in_rule__Declaration_de_fonction__Group__71330 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__Declaration_de_fonction__Group__7__Impl1358 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Declaration_de_fonction__Group_4__0__Impl_in_rule__Declaration_de_fonction__Group_4__01405 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Declaration_de_fonction__Group_4__1_in_rule__Declaration_de_fonction__Group_4__01408 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__Declaration_de_fonction__Group_4__0__Impl1436 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Declaration_de_fonction__Group_4__1__Impl_in_rule__Declaration_de_fonction__Group_4__11467 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Declaration_de_fonction__ArgumentAssignment_4_1_in_rule__Declaration_de_fonction__Group_4__1__Impl1494 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Type__Group__0__Impl_in_rule__Type__Group__01528 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Type__Group__1_in_rule__Type__Group__01531 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__Type__Group__0__Impl1559 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Type__Group__1__Impl_in_rule__Type__Group__11590 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__Type__Group__2_in_rule__Type__Group__11593 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Type__NameAssignment_1_in_rule__Type__Group__1__Impl1620 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Type__Group__2__Impl_in_rule__Type__Group__21650 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__Type__Group__2__Impl1678 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Instructions__Group_4__0__Impl_in_rule__Instructions__Group_4__01715 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__Instructions__Group_4__1_in_rule__Instructions__Group_4__01718 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Instructions__NameAssignment_4_0_in_rule__Instructions__Group_4__0__Impl1745 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Instructions__Group_4__1__Impl_in_rule__Instructions__Group_4__11775 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__Instructions__Group_4__1__Impl1803 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Instanciation__Group__0__Impl_in_rule__Instanciation__Group__01838 = new BitSet(new long[]{0x0000000000010010L});
    public static final BitSet FOLLOW_rule__Instanciation__Group__1_in_rule__Instanciation__Group__01841 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__Instanciation__Group__0__Impl1870 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Instanciation__Group__1__Impl_in_rule__Instanciation__Group__11903 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_rule__Instanciation__Group__2_in_rule__Instanciation__Group__11906 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Instanciation__NameAssignment_1_in_rule__Instanciation__Group__1__Impl1933 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Instanciation__Group__2__Impl_in_rule__Instanciation__Group__21963 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Instanciation__Group__3_in_rule__Instanciation__Group__21966 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__Instanciation__Group__2__Impl1994 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Instanciation__Group__3__Impl_in_rule__Instanciation__Group__32025 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__Instanciation__Group__4_in_rule__Instanciation__Group__32028 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Instanciation__VariableAssignment_3_in_rule__Instanciation__Group__3__Impl2055 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Instanciation__Group__4__Impl_in_rule__Instanciation__Group__42085 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__Instanciation__Group__4__Impl2113 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Addition__Group__0__Impl_in_rule__Addition__Group__02154 = new BitSet(new long[]{0x0000000000010010L});
    public static final BitSet FOLLOW_rule__Addition__Group__1_in_rule__Addition__Group__02157 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__Addition__Group__0__Impl2186 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Addition__Group__1__Impl_in_rule__Addition__Group__12219 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_rule__Addition__Group__2_in_rule__Addition__Group__12222 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Addition__NameAssignment_1_in_rule__Addition__Group__1__Impl2249 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Addition__Group__2__Impl_in_rule__Addition__Group__22279 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Addition__Group__3_in_rule__Addition__Group__22282 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__Addition__Group__2__Impl2310 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Addition__Group__3__Impl_in_rule__Addition__Group__32341 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_rule__Addition__Group__4_in_rule__Addition__Group__32344 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Addition__Variable1Assignment_3_in_rule__Addition__Group__3__Impl2371 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Addition__Group__4__Impl_in_rule__Addition__Group__42401 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Addition__Group__5_in_rule__Addition__Group__42404 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__Addition__Group__4__Impl2432 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Addition__Group__5__Impl_in_rule__Addition__Group__52463 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__Addition__Group__6_in_rule__Addition__Group__52466 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Addition__Variable2Assignment_5_in_rule__Addition__Group__5__Impl2493 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Addition__Group__6__Impl_in_rule__Addition__Group__62523 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__Addition__Group__6__Impl2551 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Alert__Group__0__Impl_in_rule__Alert__Group__02596 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Alert__Group__1_in_rule__Alert__Group__02599 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__Alert__Group__0__Impl2627 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Alert__Group__1__Impl_in_rule__Alert__Group__12658 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_rule__Alert__Group__2_in_rule__Alert__Group__12661 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Alert__MessageAssignment_1_in_rule__Alert__Group__1__Impl2688 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Alert__Group__2__Impl_in_rule__Alert__Group__22718 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__Alert__Group__2__Impl2746 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Prompt__Group__0__Impl_in_rule__Prompt__Group__02783 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Prompt__Group__1_in_rule__Prompt__Group__02786 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__Prompt__Group__0__Impl2814 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Prompt__Group__1__Impl_in_rule__Prompt__Group__12845 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_rule__Prompt__Group__2_in_rule__Prompt__Group__12848 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Prompt__Votre_textAssignment_1_in_rule__Prompt__Group__1__Impl2875 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Prompt__Group__2__Impl_in_rule__Prompt__Group__22905 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Prompt__Group__3_in_rule__Prompt__Group__22908 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__Prompt__Group__2__Impl2936 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Prompt__Group__3__Impl_in_rule__Prompt__Group__32967 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_rule__Prompt__Group__4_in_rule__Prompt__Group__32970 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Prompt__Text_par_defautAssignment_3_in_rule__Prompt__Group__3__Impl2997 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Prompt__Group__4__Impl_in_rule__Prompt__Group__43027 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__Prompt__Group__4__Impl3055 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleAbstractElement_in_rule__Domainmodel__ElementsAssignment3101 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Declaration_de_fonction__FunctionNameAssignment_13132 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Declaration_de_fonction__ArgumentsAssignment_33163 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Declaration_de_fonction__ArgumentAssignment_4_13194 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulefunctionContent_in_rule__Declaration_de_fonction__ElementAssignment_63225 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Type__NameAssignment_13256 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Instructions__NameAssignment_4_03287 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Instanciation__NameAssignment_13318 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Instanciation__VariableAssignment_33349 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Addition__NameAssignment_13380 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Addition__Variable1Assignment_33411 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Addition__Variable2Assignment_53442 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Alert__MessageAssignment_13473 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Prompt__Votre_textAssignment_13504 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Prompt__Text_par_defautAssignment_33535 = new BitSet(new long[]{0x0000000000000002L});

}